


all:
	  cd src/core  ; rm -rf ~/.tuxcmd ;  make 

oldrun:
	  cd src/core  ; rm -rf ~/.tuxcmd ;  make  ; ./tuxcmd 

fast:
	  cd src/core  ; rm -rf ~/.tuxcmd ;  make  fast 


lib:
	   apt-get install -y fpc gcc g++ make  clang git 
	   apt-get install -y fpc gcc g++ make  clang libgtk2.0-dev 

clean:
	  cd src/core  ; make clean 


compile: 
	  cd src/core  ; rm -rf ~/.tuxcmd ;  make 



ed: 
	  cd src/core  ; make ed  


ins: 
	  mv src/core/filecommander /usr/local/bin/

install: 
	  cd src/core  ; make install

run: 
	  cd src/core  ; make run 


conf: 
	   vim src/core/UConfig.pas


ls: 
	  ls -ltra --color src/core/




