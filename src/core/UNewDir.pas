(*
    Tux Commander - UNewDir - New Directory form and related functions
    Copyright (C) 2004 Tomas Bzatek <tbzatek@users.sourceforge.net>
    Check for updates on tuxcmd.sourceforge.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
unit UNewDir;

interface

uses
  SysUtils, Types, Classes, Variants, GTKControls, GTKForms, GTKStdCtrls, GTKExtCtrls, GTKConsts;

type
  TFNewDir = class(TGTKDialog)
    Label1: TGTKLabel;
    Entry: TGTKEntry;
    Box: TGTKVBox;
    procedure FormCreate(Sender: TObject); override;
    procedure FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FNewDir: TFNewDir;

implementation

uses ULocale;


procedure TFNewDir.FormCreate(Sender: TObject);
begin
  SetDefaultSize(350, -1);
  Caption := LANGCreateANewDirectory;
  Buttons := [mbOK, mbCancel];
  Box := TGTKVBox.Create(Self);
  Label1 := TGTKLabel.Create(Self);
  Label1.Caption := LANGEnterDirectoryName;
  Label1.UseUnderline := True;
  Label1.XAlign := 0;
  Label1.XPadding := 0;
  Label1.LineWrap := True;
  Entry := TGTKEntry.Create(Self);
  Label1.FocusControl := Entry;
  Box.AddControlEx(Label1, False, False, 2);
  Box.AddControlEx(Entry, False, False, 0);
  Box.BorderWidth := 6;
  ClientArea.AddControlEx(Box, True, True, 0);
  OnKeyDown := FormKeyDown;
  Entry.SetFocus;
end;

procedure TFNewDir.FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
begin
  case Key of
    GDK_RETURN, GDK_KP_ENTER: ModalResult := mbOK;
    GDK_ESCAPE: ModalResult := mbCancel;
  end;
end;


end.

