(*
    GTK-Kylix Library: GTKUtils - Various utilities
    Version 0.6.17  (last updated 2003-10-07)
    Copyright (C) 2003 Tomas Bzatek <tbzatek@users.sourceforge.net>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the 
    Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
    Boston, MA  02111-1307  USA.

*)

unit GTKUtils;

interface

uses gtk2, gdk2, glib2, SysUtils, GTKControls, GTKClasses;

(********************************************************************************************************************************)
function PgcharToString(const S: Pgchar): string;
function StringToPgchar(const S: string): Pgchar;
function AllocateColor(Widget: PGtkWidget; R, G, B: Word): PGdkColor; overload;
function AllocateColor(R, G, B: Word): TGDKColor; overload;
function KeyValToUnicode(const Key: word): guint32;
function UnicodeToKeyVal(const UnicodeVal: guint32): word;
function GetDefaultForegroundColor(State: integer): PGdkColor; overload;
function GetDefaultForegroundColor(Widget: TGTKControl; State: integer): PGdkColor; overload;
function GetDefaultBackgroundColor(State: integer): PGdkColor; overload;
function GetDefaultBackgroundColor(Widget: TGTKControl; State: integer): PGdkColor; overload;
function GetDefaultBaseColor(State: integer): PGdkColor; overload;
function GetDefaultBaseColor(Widget: TGTKControl; State: integer): PGdkColor; overload;
function GetDefaultTextColor(State: integer): PGdkColor; overload;
function GetDefaultTextColor(Widget: TGTKControl; State: integer): PGdkColor; overload;
function GDKColorToPGdkColor(Color: TGDKColor): PGdkColor;
function PGdkColorToGDKColor(Color: PGdkColor): TGDKColor;
function GDKColorToString(Color: TGDKColor): string;
function StringToGDKColor(Str: string; var Color: TGDKColor): boolean;
(********************************************************************************************************************************)

implementation

(********************************************************************************************************************************)
function PgcharToString(const S: Pgchar): string;
begin
  Result := string(S);
end;

(********************************************************************************************************************************)
function StringToPgchar(const S: string): Pgchar;
begin
  Result := PChar(S);
end;

(********************************************************************************************************************************)
function AllocateColor(Widget: PGtkWidget; R, G, B: Word): PGdkColor;
begin
  New(Result);
  with Result^ do begin
    Pixel := 0;
    Red := R;
    Green := G;
    Blue := B;
  end;
  if Assigned(Widget) then gdk_colormap_alloc_color(gtk_widget_get_colormap(Widget), Result, True, False);
end;

function AllocateColor(R, G, B: Word): TGDKColor;
begin
  Result.red := R;
  Result.green := G;
  Result.blue := B;
end;

(********************************************************************************************************************************)
function KeyValToUnicode(const Key: word): guint32;
begin
  Result := gdk_keyval_to_unicode(Key);
end;

(********************************************************************************************************************************)
function UnicodeToKeyVal(const UnicodeVal: guint32): word;
begin
  Result := gdk_unicode_to_keyval(UnicodeVal);
end;

(********************************************************************************************************************************)
function GetDefaultForegroundColor(State: integer): PGdkColor;
var Widget: PGtkWidget;
    Style: PGtkStyle;
begin
  Widget := gtk_window_new(GTK_WINDOW_TOPLEVEL);
  Style := gtk_rc_get_style(Widget);
  Result := @Style^.fg[State];
  gtk_widget_destroy(Widget);
end;

function GetDefaultForegroundColor(Widget: TGTKControl; State: integer): PGdkColor;
var Style: PGtkStyle;
begin
  Style := gtk_rc_get_style(Widget.FWidget);
  Result := @Style^.fg[State];
end;

(********************************************************************************************************************************)
function GetDefaultBackgroundColor(State: integer): PGdkColor;
var Widget: PGtkWidget;
    Style: PGtkStyle;
begin
  Widget := gtk_window_new(GTK_WINDOW_TOPLEVEL);
  Style := gtk_rc_get_style(Widget);
  Result := @Style^.bg[State];
  gtk_widget_destroy(Widget);
end;

function GetDefaultBackgroundColor(Widget: TGTKControl; State: integer): PGdkColor;
var Style: PGtkStyle;
begin
  Style := gtk_rc_get_style(Widget.FWidget);
  Result := @Style^.bg[State];
end;

(********************************************************************************************************************************)
function GetDefaultBaseColor(State: integer): PGdkColor;
var Widget: PGtkWidget;
    Style: PGtkStyle;
begin
  Widget := gtk_window_new(GTK_WINDOW_TOPLEVEL);
  Style := gtk_rc_get_style(Widget);
  Result := @Style^.base[State];
  gtk_widget_destroy(Widget);
end;

function GetDefaultBaseColor(Widget: TGTKControl; State: integer): PGdkColor;
var Style: PGtkStyle;
begin
  Style := gtk_rc_get_style(Widget.FWidget);
  Result := @Style^.base[State];
end;

(********************************************************************************************************************************)
function GetDefaultTextColor(State: integer): PGdkColor;
var Widget: PGtkWidget;
    Style: PGtkStyle;
begin
  Widget := gtk_window_new(GTK_WINDOW_TOPLEVEL);
  Style := gtk_rc_get_style(Widget);
  Result := @Style^.text[State];
  gtk_widget_destroy(Widget);
end;

function GetDefaultTextColor(Widget: TGTKControl; State: integer): PGdkColor;
var Style: PGtkStyle;
begin
  Style := gtk_rc_get_style(Widget.FWidget);
  Result := @Style^.text[State];
end;

(********************************************************************************************************************************)
function GDKColorToPGdkColor(Color: TGDKColor): PGdkColor;
begin
  New(Result);
  Result^.pixel := Color.pixel;
  Result^.red := Color.red;
  Result^.green := Color.green;
  Result^.blue := Color.blue;
end;

(********************************************************************************************************************************)
function PGdkColorToGDKColor(Color: PGdkColor): TGDKColor;
begin
  Result.pixel := Color.pixel;
  Result.red := Color.red;
  Result.green := Color.green;
  Result.blue := Color.blue;
end;

(********************************************************************************************************************************)
function GDKColorToString(Color: TGDKColor): string;
begin
  Result := Format('#%.2x%.2x%.2xFF', [Color.red div 256, Color.green div 256, Color.blue div 256]);
end;

(********************************************************************************************************************************)
function StringToGDKColor(Str: string; var Color: TGDKColor): boolean;
var AColor: PGdkColor;
begin
  Result := False;
  Str := ANSIUpperCase(Trim(Str));
  if (Length(Str) < 7) or (Str[1] <> '#') then Exit;
  try
    New(AColor);
    if Length(Str) = 9 then Delete(Str, 8, 2); 
    Result := boolean(gdk_color_parse(PChar(Str), AColor));
    Color := PGdkColorToGDKColor(AColor);
    Dispose(AColor);
  except end;
end;

(********************************************************************************************************************************)

end.
