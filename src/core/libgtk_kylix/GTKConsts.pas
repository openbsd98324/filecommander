(*
    GTK-Kylix Library: GTKConsts - Various constants, mainly key codes
    Version 0.5.20  (last updated 2003-01-25)
    Copyright (C) 2003 Tomas Bzatek <tbzatek@users.sourceforge.net>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the 
    Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
    Boston, MA  02111-1307  USA.

*)

unit GTKConsts;

interface

uses glib2, gdk2, gtk2;

const   //  copied from gdkkeysyms
   GDK_BackSpace = $FF08;
   GDK_Tab = $FF09;
   GDK_Linefeed = $FF0A;
   GDK_Clear_Key = $FF0B;
   GDK_Return = $FF0D;
   GDK_Pause = $FF13;
   GDK_Scroll_Lock = $FF14;
   GDK_Sys_Req = $FF15;
   GDK_Escape = $FF1B;
   GDK_Delete_Key = $FFFF;
   GDK_Home = $FF50;
   GDK_Left = $FF51;
   GDK_Up = $FF52;
   GDK_Right = $FF53;
   GDK_Down = $FF54;
   GDK_Prior = $FF55;
   GDK_Page_Up = $FF55;
   GDK_Next = $FF56;
   GDK_Page_Down = $FF56;
   GDK_End = $FF57;
   GDK_Insert = $FF63;
   GDK_Num_Lock = $FF7F;
   GDK_KP_Space = $FF80;
   GDK_KP_Tab = $FF89;
   GDK_KP_Enter = $FF8D;
   GDK_KP_F1 = $FF91;
   GDK_KP_F2 = $FF92;
   GDK_KP_F3 = $FF93;
   GDK_KP_F4 = $FF94;
   GDK_KP_Home = $FF95;
   GDK_KP_Left = $FF96;
   GDK_KP_Up = $FF97;
   GDK_KP_Right = $FF98;
   GDK_KP_Down = $FF99;
   GDK_KP_Prior = $FF9A;
   GDK_KP_Page_Up = $FF9A;
   GDK_KP_Next = $FF9B;
   GDK_KP_Page_Down = $FF9B;
   GDK_KP_End = $FF9C;
   GDK_KP_PLUS = 65451;
   GDK_KP_MINUS = 65453;
   GDK_KP_ASTERISK = 65450;
   GDK_KP_SLASH = 65455;
   GDK_KP_0 = $FFB0;
   GDK_KP_1 = $FFB1;
   GDK_KP_2 = $FFB2;
   GDK_KP_3 = $FFB3;
   GDK_KP_4 = $FFB4;
   GDK_KP_5 = $FFB5;
   GDK_KP_6 = $FFB6;
   GDK_KP_7 = $FFB7;
   GDK_KP_8 = $FFB8;
   GDK_KP_9 = $FFB9;
   GDK_F1 = $FFBE;
   GDK_F2 = $FFBF;
   GDK_F3 = $FFC0;
   GDK_F4 = $FFC1;
   GDK_F5 = $FFC2;
   GDK_F6 = $FFC3;
   GDK_F7 = $FFC4;
   GDK_F8 = $FFC5;
   GDK_F9 = $FFC6;
   GDK_F10 = $FFC7;
   GDK_F11 = $FFC8;
   GDK_L1 = $FFC8;
   GDK_F12 = $FFC9;
   GDK_L2 = $FFC9;
   GDK_F13 = $FFCA;
   GDK_L3 = $FFCA;
   GDK_F14 = $FFCB;
   GDK_L4 = $FFCB;
   GDK_F15 = $FFCC;
   GDK_L5 = $FFCC;
   GDK_F16 = $FFCD;
   GDK_L6 = $FFCD;
   GDK_F17 = $FFCE;
   GDK_L7 = $FFCE;
   GDK_F18 = $FFCF;
   GDK_L8 = $FFCF;
   GDK_F19 = $FFD0;
   GDK_L9 = $FFD0;
   GDK_F20 = $FFD1;
   GDK_L10 = $FFD1;
   GDK_F21 = $FFD2;
   GDK_R1 = $FFD2;
   GDK_F22 = $FFD3;
   GDK_R2 = $FFD3;
   GDK_F23 = $FFD4;
   GDK_R3 = $FFD4;
   GDK_F24 = $FFD5;
   GDK_R4 = $FFD5;
   GDK_F25 = $FFD6;
   GDK_R5 = $FFD6;
   GDK_F26 = $FFD7;
   GDK_R6 = $FFD7;
   GDK_F27 = $FFD8;
   GDK_R7 = $FFD8;
   GDK_F28 = $FFD9;
   GDK_R8 = $FFD9;
   GDK_F29 = $FFDA;
   GDK_R9 = $FFDA;
   GDK_F30 = $FFDB;
   GDK_R10 = $FFDB;
   GDK_F31 = $FFDC;
   GDK_R11 = $FFDC;
   GDK_F32 = $FFDD;
   GDK_R12 = $FFDD;
   GDK_F33 = $FFDE;
   GDK_R13 = $FFDE;
   GDK_F34 = $FFDF;
   GDK_R14 = $FFDF;
   GDK_F35 = $FFE0;
   GDK_R15 = $FFE0;
   GDK_space = $020;
   GDK_exclam = $021;
   GDK_quotedbl = $022;
   GDK_numbersign = $023;
   GDK_dollar = $024;
   GDK_percent = $025;
   GDK_ampersand = $026;
   GDK_apostrophe = $027;
   GDK_quoteright = $027;
   GDK_parenleft = $028;
   GDK_parenright = $029;
   GDK_asterisk = $02a;
   GDK_plus_key = $02b;
   GDK_comma = $02c;
   GDK_minus = $02d;
   GDK_period = $02e;
   GDK_slash = $02f;
   GDK_0 = $030;
   GDK_1 = $031;
   GDK_2 = $032;
   GDK_3 = $033;
   GDK_4 = $034;
   GDK_5 = $035;
   GDK_6 = $036;
   GDK_7 = $037;
   GDK_8 = $038;
   GDK_9 = $039;
   GDK_colon = $03a;
   GDK_semicolon = $03b;
   GDK_less = $03c;
   GDK_equal = $03d;
   GDK_greater = $03e;
   GDK_question = $03f;
   GDK_at = $040;
   GDK_Capital_A = $041;
   GDK_Capital_B = $042;
   GDK_Capital_C = $043;
   GDK_Capital_D = $044;
   GDK_Capital_E = $045;
   GDK_Capital_F = $046;
   GDK_Capital_G = $047;
   GDK_Capital_H = $048;
   GDK_Capital_I = $049;
   GDK_Capital_J = $04a;
   GDK_Capital_K = $04b;
   GDK_Capital_L = $04c;
   GDK_Capital_M = $04d;
   GDK_Capital_N = $04e;
   GDK_Capital_O = $04f;
   GDK_Capital_P = $050;
   GDK_Capital_Q = $051;
   GDK_Capital_R = $052;
   GDK_Capital_S = $053;
   GDK_Capital_T = $054;
   GDK_Capital_U = $055;
   GDK_Capital_V = $056;
   GDK_Capital_W = $057;
   GDK_Capital_X = $058;
   GDK_Capital_Y = $059;
   GDK_Capital_Z = $05a;
   GDK_bracketleft = $05b;
   GDK_backslash = $05c;
   GDK_bracketright = $05d;
   GDK_quoteleft = $060;
   GDK_a = $061;
   GDK_b = $062;
   GDK_c = $063;
   GDK_d = $064;
   GDK_e = $065;
   GDK_f = $066;
   GDK_g = $067;
   GDK_h = $068;
   GDK_i = $069;
   GDK_j = $06a;
   GDK_k = $06b;
   GDK_l = $06c;
   GDK_m = $06d;
   GDK_n = $06e;
   GDK_o = $06f;
   GDK_p = $070;
   GDK_q = $071;
   GDK_r = $072;
   GDK_s = $073;
   GDK_t = $074;
   GDK_u = $075;
   GDK_v = $076;
   GDK_w = $077;
   GDK_x = $078;
   GDK_y = $079;
   GDK_z = $07a;
   GDK_aacute = $0e1;
   GDK_leftarrow = $8fb;
   GDK_uparrow = $8fc;
   GDK_rightarrow = $8fd;
   GDK_downarrow = $8fe;
   GDK_WIN_POPUP = $FF67;

   NumMessageButtons = 12;
   MessageButtonID : array[1..NumMessageButtons] of Pgchar =
       (GTK_STOCK_YES, GTK_STOCK_NO, GTK_STOCK_OK, GTK_STOCK_CANCEL, GTK_STOCK_APPLY, GTK_STOCK_CLOSE, GTK_STOCK_HELP,
        GTK_STOCK_PRINT, GTK_STOCK_QUIT, GTK_STOCK_STOP, GTK_STOCK_GO_BACK, GTK_STOCK_GO_FORWARD);

resourcestring
  SCDefaultMenuItemCaption = 'MenuItem';
  SCDefaultButtonCaption = 'Button';
  SCDefaultLabelCaption = 'Label';
  SCDefaultToggleButtonCaption = 'ToggleButton';
  SCDefaultCheckButtonCaption = 'CheckButton';
  SCDefaultRadioButtonCaption = 'RadioButton';
  SCDefaultFrameCaption = 'Frame';


implementation


end.
