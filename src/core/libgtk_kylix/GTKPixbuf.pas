(*
    GTK-Kylix Library: GTKPixbuf - Image handling routines
    Version 0.6.2  (last updated 2003-03-30)
    Copyright (C) 2003 Tomas Bzatek <tbzatek@users.sourceforge.net>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the
    Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA  02111-1307  USA.

*)

unit GTKPixbuf;

interface

uses glib2, gdk2, gdk2pixbuf, gtk2, Classes, GTKControls, GTKStdCtrls;


type
  TGTKIconSize = (isInvalid, isMenu, isSmallToolbar, isLargeToolbar, isButton, isDND, isDialog);
  
(****************************************** TGDKPIXBUF **************************************************************************)
  TGDKPixbuf = class (TComponent)
  private
    function GetWidth: integer;
    function GetHeight: integer;
    function GetBPP: integer;
  protected
  public
    FPixbuf: PGdkPixbuf;
    constructor Create(AOwner: TComponent); override;
    constructor CreateNew(AOwner: TComponent; const Width, Height, BPP: integer; const HasAlpha: boolean);
    destructor Destroy; override;
    function LoadFromFile(const FileName: string): boolean;
    function LoadFromXPM(const Data: PPChar): boolean;
    function LoadFromInline(Data: Pointer): boolean;
    procedure Fill(const Pixel: Cardinal);
    procedure ScaleSimple(const DestWidth, DestHeight: integer);
    function Copy: PGdkPixbuf;
    procedure CopyArea(Source: TGDKPixbuf; SourceX, SourceY, SourceWidth, SourceHeight, DestX, DestY: integer);
    procedure SetFromStock(Stock_ID: string; IconSize: TGTKIconSize);
  published
    property Width: integer read GetWidth;
    property Height: integer read GetHeight;
    property BPP: integer read GetBPP;
  end;

(****************************************** TGTKIMAGE ***************************************************************************)
  TGTKImage = class(TGTKMisc)
  private
  protected
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure LoadFromFile(const FileName: string);
    procedure SetFromPixbuf(Pixbuf: TGDKPixbuf);
    function GetPixbuf: PGdkPixbuf;
    procedure CopyFromPixbuf(Pixbuf: TGDKPixbuf);
    procedure SetFromStock(Stock_ID: string; IconSize: TGTKIconSize);
  end;

(********************************************************************************************************************************)
(********************************************************************************************************************************)
implementation

uses SysUtils, DateUtils;


(********************************************************************************************************************************)
(********************************************************************************************************************************)
constructor TGDKPixbuf.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FPixbuf := nil;
end;

constructor TGDKPixbuf.CreateNew(AOwner: TComponent; const Width, Height, BPP: integer; const HasAlpha: boolean);
begin
  inherited Create(AOwner);
  FPixbuf := gdk_pixbuf_new(GDK_COLORSPACE_RGB, HasAlpha, BPP, Width, Height);
end;

destructor TGDKPixbuf.Destroy;
begin
  if FPixbuf <> nil then gdk_pixbuf_unref(FPixbuf);
  inherited Destroy;
end;

function TGDKPixbuf.GetWidth: integer;
begin
  Result := gdk_pixbuf_get_width(FPixbuf);
end;

function TGDKPixbuf.GetHeight: integer;
begin
  Result := gdk_pixbuf_get_height(FPixbuf);
end;

function TGDKPixbuf.GetBPP: integer;
begin
  Result := gdk_pixbuf_get_bits_per_sample(FPixbuf);
end;

function TGDKPixbuf.LoadFromFile(const FileName: string): boolean;
var P: Pointer;
    Error: PGError;
begin
  Error := nil;
  P := gdk_pixbuf_new_from_file(PChar(FileName), @Error);
  Result := P <> nil;
  if P <> nil then FPixbuf := P;
end;

function TGDKPixbuf.LoadFromXPM(const Data: PPChar): boolean;
var P: Pointer;
begin
  P := gdk_pixbuf_new_from_xpm_data(Data);
  Result := P <> nil;
  if P <> nil then FPixbuf := P;
end;

function TGDKPixbuf.LoadFromInline(Data: Pointer): boolean;
var P: Pointer;
    Error: PGError;
begin
  Error := nil;
  P := gdk_pixbuf_new_from_inline(-1, Pguint8(Data)^, True, @Error);
  Result := P <> nil;
  if Error <> nil then begin
    WriteLn('TGDKPixbuf.LoadFromInline error: ', Error^.message);
    g_error_free(Error);
  end;
  if P <> nil then FPixbuf := P;
end;

procedure TGDKPixbuf.Fill(const Pixel: Cardinal);
begin
  gdk_pixbuf_fill(FPixbuf, Pixel);
end;

procedure TGDKPixbuf.ScaleSimple(const DestWidth, DestHeight: integer);
begin
  FPixbuf := gdk_pixbuf_scale_simple(FPixbuf, DestWidth, DestHeight, GDK_INTERP_BILINEAR);
end;

function TGDKPixbuf.Copy: PGdkPixbuf;
begin
  Result := gdk_pixbuf_copy(FPixbuf);
end;

procedure TGDKPixbuf.CopyArea(Source: TGDKPixbuf; SourceX, SourceY, SourceWidth, SourceHeight, DestX, DestY: integer);
begin
  gdk_pixbuf_copy_area(Source.FPixbuf, SourceX, SourceY, SourceWidth, SourceHeight, FPixbuf, DestX, DestY);
end;

procedure TGDKPixbuf.SetFromStock(Stock_ID: string; IconSize: TGTKIconSize);
begin
  FPixbuf := gtk_widget_render_icon(gtk_label_new(nil), PChar(Stock_ID), Ord(IconSize), nil);
end;

    
(********************************************************************************************************************************)
(********************************************************************************************************************************)
constructor TGTKImage.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FWidget := gtk_image_new;
  Show;
end;

destructor TGTKImage.Destroy;
begin
  inherited Destroy;
end;

procedure TGTKImage.LoadFromFile(const FileName: string);
begin
  gtk_image_set_from_file(PGtkImage(FWidget), PChar(FileName));
end;

function TGTKImage.GetPixbuf: PGdkPixbuf;
begin
  Result := gtk_image_get_pixbuf(PGtkImage(FWidget));
end;

procedure TGTKImage.SetFromPixbuf(Pixbuf: TGDKPixbuf);
begin
  gtk_image_set_from_pixbuf(PGtkImage(FWidget), Pixbuf.FPixbuf);
end;

procedure TGTKImage.CopyFromPixbuf(Pixbuf: TGDKPixbuf);
begin
  gtk_image_set_from_pixbuf(PGtkImage(FWidget), Pixbuf.Copy);
end;

procedure TGTKImage.SetFromStock(Stock_ID: string; IconSize: TGTKIconSize);
begin
  gtk_image_set_from_stock(PGtkImage(FWidget), PChar(Stock_ID), Ord(IconSize));
end;

(********************************************************************************************************************************)
end.
