(*
    Tux Commander - URemoteWait - Dialog shown while doing some remote actions
    Copyright (C) 2004 Tomas Bzatek <tbzatek@users.sourceforge.net>
    Check for updates on tuxcmd.sourceforge.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
unit URemoteWait;

interface

uses
  glib2, gdk2, gtk2, pango,
  SysUtils, Types, Classes, Variants, GTKControls, GTKForms, GTKStdCtrls, GTKExtCtrls, GTKConsts;

type
  TFRemoteWait = class(TGTKDialog)
    Label1{, Label2}: TGTKLabel;
    Entry: TGTKEntry;
    Box: TGTKVBox;
    CancelButton: TGTKButton;
    procedure FormCreate(Sender: TObject); override;
    procedure FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CancelButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    Cancelled: boolean;
  end;

var
  FRemoteWait: TFRemoteWait;

implementation

uses ULocale;


procedure TFRemoteWait.FormCreate(Sender: TObject);
begin
  SetDefaultSize(350, -1);
  WindowPosition := wpCenterAlways;
//  WindowTypeHint := whDialog;
  Caption := LANGRemoteWait_Caption;
  Buttons := [];
  Box := TGTKVBox.Create(Self);
  Label1 := TGTKLabel.Create(Self);
  Label1.Caption := Format('<span weight="ultrabold">%s</span>', [LANGRemoteWait_OperationInProgress]);
  Label1.UseUnderline := True;
  Label1.UseMarkup := True;
  Label1.XAlign := 0;
  Label1.XPadding := 0;
  Label1.LineWrap := True;
  Box.AddControlEx(Label1, False, False, 2);
{
  Label2 := TGTKLabel.Create(Self);
  Label2.Caption := LANGRemoteWait_ItemsFound;
  Label2.XAlign := 0;
  Label2.XPadding := 20;
  Label2.LineWrap := True;
  Box.AddControlEx(Label2, False, False, 7);
}
  Box.BorderWidth := 6;
  ClientArea.AddControlEx(Box, True, True, 0);
  CancelButton := TGTKButton.CreateFromStock(Self, 'gtk-cancel');
  CancelButton.Default := True;
  ActionArea.AddControlEndEx(CancelButton, False, False, 2);
  OnKeyDown := FormKeyDown;
  OnCloseQuery := FormCloseQuery;
  CancelButton.OnClick := CancelButtonClick;
  Cancelled := False;
end;

procedure TFRemoteWait.FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
begin
  case Key of
    GDK_RETURN, GDK_KP_ENTER, GDK_ESCAPE: Cancelled := True;
  end;
end;

procedure TFRemoteWait.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := False;
  Cancelled := True;
end;

procedure TFRemoteWait.CancelButtonClick(Sender: TObject);
begin
  Cancelled := True;
end;

end.

