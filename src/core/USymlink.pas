(*
    Tux Commander - USymlink - Symbolic link 
    Copyright (C) 2004 Tomas Bzatek <tbzatek@users.sourceforge.net>
    Check for updates on tuxcmd.sourceforge.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
unit USymlink;

interface

uses
  SysUtils, Types, Classes, Variants, GTKControls, GTKForms, GTKStdCtrls, GTKExtCtrls, GTKConsts;

type
  TFSymlink = class(TGTKDialog)
    Label1, Label2: TGTKLabel;
    FromEntry, ToEntry: TGTKEntry;
    Box: TGTKVBox;
    RelativeCheckButton: TGTKCheckButton;
    procedure FormCreate(Sender: TObject); override;
    procedure FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
    procedure RelativeCheckButtonToggled(Sender: TObject);
  private
    { Private declarations }
  public
    FileName, PossibleNewName: string;
  end;

var
  FSymlink: TFSymlink;

implementation

uses ULocale, UCoreUtils;


procedure TFSymlink.FormCreate(Sender: TObject);
begin
  SetDefaultSize(500, -1);
  Caption := LANGFSymlink_Caption;
  Buttons := [mbOK, mbCancel];
  Box := TGTKVBox.Create(Self);
  Label1 := TGTKLabel.Create(Self);
  Label1.Caption := LANGFSymlink_ExistingFilename;
  Label1.UseUnderline := True;
  Label1.XAlign := 0;
  Label1.XPadding := 0; 
  FromEntry := TGTKEntry.Create(Self);
  Label1.FocusControl := FromEntry;
  Label2 := TGTKLabel.Create(Self);
  Label2.Caption := LANGFSymlink_SymlinkFilename;
  Label2.UseUnderline := True;
  Label2.XAlign := 0;
  Label2.XPadding := 0;
  ToEntry := TGTKEntry.Create(Self);
  Label2.FocusControl := ToEntry;
  RelativeCheckButton := TGTKCheckButton.CreateWithLabel(Self, LANGFSymlink_RelativePath);
  RelativeCheckButton.OnToggled := RelativeCheckButtonToggled;
  Box.AddControlEx(Label1, False, False, 2);
  Box.AddControlEx(FromEntry, False, False, 0);
  Box.AddControlEx(RelativeCheckButton, False, False, 0);
  Box.AddControlEx(TGTKHSeparator.Create(Self), False, False, 5);
  Box.AddControlEx(Label2, False, False, 2);
  Box.AddControlEx(ToEntry, False, False, 0);
  Box.BorderWidth := 10;
  ClientArea.AddControlEx(Box, True, True, 0);
  OnKeyDown := FormKeyDown;
  ToEntry.SetFocus;
end;

procedure TFSymlink.FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
begin
  case Key of
    GDK_RETURN, GDK_KP_ENTER: ModalResult := mbOK;
    GDK_ESCAPE: ModalResult := mbCancel;
  end;
end;

procedure TFSymlink.RelativeCheckButtonToggled(Sender: TObject);
begin
  if RelativeCheckButton.Checked then FromEntry.Text := StrToUTF8(BuildRelativePath(FileName, PossibleNewName))
                                 else FromEntry.Text := StrToUTF8(FileName);
end;


end.

