(*
    Tux Commander - UDirDelete - Question dialog and related funcions 
    Copyright (C) 2004 Tomas Bzatek <tbzatek@users.sourceforge.net>
    Check for updates on tuxcmd.sourceforge.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
unit UDirDelete;

interface

uses
  SysUtils, Types, Classes, Variants, GTKControls, GTKForms, GTKStdCtrls, GTKExtCtrls, GTKConsts;

type
  TFDirDelete = class(TGTKDialog)
    Label1, Label2, Label3: TGTKLabel;
    procedure FormCreate(Sender: TObject); override;
    procedure FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
  public
    procedure AddButtons(Sel: integer);
  end;

var
  FDirDelete: TFDirDelete;

implementation

uses ULocale;


procedure TFDirDelete.FormCreate(Sender: TObject);
begin
  WindowPosition := wpCenter;
  Caption := LANGRemoveDirectory;
  Label1 := TGTKLabel.Create(Self);
  Label1.Caption := 'The directory /tmp is not empty!';
  Label2 := TGTKLabel.Create(Self);
  Label2.Caption := LANGDoYouWantToDeleteItWithAllItsFilesAndSubdirectories;
  Label3 := TGTKLabel.Create(Self);
  Label3.Visible := False;
  ClientArea.AddControlEx(Label1, True, True, 0);
  ClientArea.AddControlEx(Label2, True, True, 0);
  ClientArea.AddControlEx(Label3, True, True, 0);
  OnKeyDown := FormKeyDown;
end;

procedure TFDirDelete.AddButtons(Sel: integer);
begin
  case Sel of
    1 : begin
          AddButton(LANGSkipButton_Caption, 1);
          AddButton(LANGSkipAllButton_Caption, 3);
          AddButton(LANGRetry, 2);
          AddButton(LANGCancel, 0);
        end;
    2 : begin
          AddButton(LANGRetry, 1);
          AddButton(LANGCancel, 0);
        end;
    3 : begin
          AddButton(LANGSkipButton_Caption, 1);
          AddButton(LANGSkipAllButton_Caption, 3);
          AddButton(LANGIgnoreButton_Caption, 2);
          AddButton(LANGCancel, 0);
        end;
    4 : begin
          AddButton(LANGDeleteButton_Caption, 1);
          AddButton(LANGAll, 2);
          AddButton(LANGSkipButton_Caption, 3);
          AddButton(LANGCancel, 0);
        end;
  end;
end;

procedure TFDirDelete.FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
begin
  case Key of
    GDK_ESCAPE: ModalResult := TMessageButton(255);
  end;
end;




end.


