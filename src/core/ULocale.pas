(*
    Tux Commander - ULocale - Localization core
    Copyright (C) 2007 Tomas Bzatek <tbzatek@users.sourceforge.net>
    Check for updates on tuxcmd.sourceforge.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
{$J+}
unit ULocale;

interface

var LANGF2Button_Caption, LANGF3Button_Caption, LANGF4Button_Caption, LANGF5Button_Caption, LANGF6Button_Caption,
    LANGF7Button_Caption, LANGF8Button_Caption: string;
    LANGmnuFile_Caption, LANGmnuMark_Caption, LANGmnuCommands_Caption, LANGmnuHelp_Caption, LANGmiExit_Caption,
    LANGmiSelectGroup_Caption, LANGmiUnselectGroup_Caption, LANGmiSelectAll_Caption, LANGmiUnselectAll_Caption,
    LANGmiInvertSelection_Caption, LANGmiRefresh_Caption, LANGmiAbout_Caption: string;
    LANGColumn1_Caption, LANGColumn2_Caption, LANGColumn3_Caption, LANGColumn4_Caption, LANGColumn5_Caption: string;
    LANGExpandSelection, LANGShrinkSelection, LANGNoMatchesFound, LANGNoFilesSelected, LANGSelectedFilesDirectories,
    LANGDirectoryS, LANGFileS, LANGDoYouReallyWantToDeleteTheS, LANGDoYouReallyWantToDeleteTheSS, LANGCopyFiles,
    LANGMoveRenameFiles, LANGCopyDFileDirectoriesTo, LANGMoveRenameDFileDirectoriesTo, LANGCopySC, LANGMoveRenameSC: string;
    LANGQuickFind: string;

    LANGAboutString, LANGAboutStringGnome, LANGDiskStatFmt, LANGDiskStatVolNameFmt, LANGStatusLineFmt: string;
    LANGPanelStrings : array[boolean] of string;

    LANGDIR, LANGErrorGettingListingForSPanel, LANGErrorGettingListingForSPanelNoPath, LANGErrorCreatingNewDirectorySInSPanel,
    LANGErrorCreatingNewDirectorySInSPanelNoPath, LANGTheFileDirectory, LANGCouldNotBeDeleted, LANGCouldNotBeDeletedS,
    LANGUserCancelled, LANGTheDirectorySIsNotEmpty, LANGCannotCopyFile, LANGCopyError, LANGMoveError, LANGOverwriteS,
    LANGWithFileS, LANGOvewriteSBytesS, LANGTheFile, LANGCopy, LANGMove, LANGTheDirectory, LANGTheSymbolicLink,
    LANGCannotMoveFile, LANGCouldNotBeCreated, LANGCouldNotBeCreatedS, LANGFromS, LANGToS, LANGCannotCopyFileToItself,
    LANGMemoryAllocationFailed, LANGCannotOpenSourceFile, LANGCannotOpenDestinationFile, LANGCannotCloseDestinationFile,
    LANGCannotCloseSourceFile, LANGCannotReadFromSourceFile, LANGCannotWriteToDestinationFile: string;

    LANGUnknownException, LANGNoAccess, LANGUnknownError: string;

    LANGCreateANewDirectory, LANGEnterDirectoryName: string;

    LANGOverwriteQuestion, LANGOverwriteButton_Caption, LANGOverwriteAllButton_Caption, LANGSkipButton_Caption,
    LANGOverwriteAllOlderButton_Caption, LANGSkipAllButton_Caption, LANGRenameButton_Caption, LANGAppendButton_Caption,
    LANGRename, LANGRenameFile, LANGIgnoreButton_Caption: string;

    LANGProgress, LANGCancel, LANGDelete: string;

    LANGSpecifyFileType: string;

    LANGRemoveDirectory, LANGDoYouWantToDeleteItWithAllItsFilesAndSubdirectories, LANGRetry, LANGDeleteButton_Caption,
    LANGAll: string;

    LANGCopyFilesSC, LANGAppendQuestion, LANGPreparingList: string;

    LANGYouMustSelectAValidFile, LANGmiVerifyChecksums, LANGVerifyChecksumsCaption, LANGCheckButtonCaptionCheck,
    LANGCheckButtonCaptionStop, LANGFileListTooltip, LANGFilenameColumnCaption, LANGTheFileSYouAreTryingToOpenIsQuiteBig,
    LANGAnErrorOccuredWhileInitializingMemoryBlock, LANGAnErrorOccuredWhileOpeningFileSS, LANGAnErrorOccuredWhileReadingFileSS : string;
    LANGChecksumNotChecked, LANGChecksumChecking, LANGChecksumInterrupted, LANGChecksumDOK: string;

    LANGmiCreateChecksumsCaption, LANGYouMustSelectAtLeastOneFileToCalculateChecksum, LANGCreateChecksumsCaption,
    LANGCCHKSUMPage1Text, LANGCCHKSUMPage4Text, LANGCCHKSUMPage6Text, LANGCCHKSUMPage7Text, LANGCCHKSUMPage1Title,
    LANGCCHKSUMPage2Title, LANGCCHKSUMPage3Title, LANGCCHKSUMPage4Title, LANGCCHKSUMPage5Title, LANGCCHKSUMPage6Title,
    LANGCCHKSUMPage7Title, LANGCCHKSUMSFVFile, LANGCCHKSUMMD5sumFile, LANGCCHKSUMFileName, LANGCCHKSUMCreateSeparateChecksumFiles,
    LANGCCHKSUMNowProcessingFileS, LANGCCHKSUMFinishCaption, LANGCCHKSUMAreYouSureYouWantToAbortTheProcessing,
    LANGCCHKSUMAnErrorOccuredWhileOpeningFileSS,  LANGCCHKSUMAnErrorOccuredWhileReadingFileSS,
    LANGCCHKSUMAnErrorOccuredWhileWritingFileSS: string;

    LANGAnErrorOccuredWhileWritingFileSS, LANGTheTargetFileSAlreadyExistsDoYouWantToOverwriteIt, LANGTheTargetFileSCannotBeRemovedS,
    LANGMergeCaption, LANGPleaseInsertNextDiskOrGiveDifferentLocation, LANGMergeOfSSucceeded, LANGWarningCreatedFileFailsCRCCheck,
    LANGMergeOfSSucceeded_NoCRCFileAvailable, LANGMergeSAndAllFilesWithAscendingNamesToTheFollowingDirectory, LANGMergeSC,
    LANGmiSplitFileCaption, LANGmiMergeFilesCaption: string;

    LANGSplitTheFileSToDirectory, LANGSplitSC, LANGSplitFile, LANGBytesPerFile, LANGAutomatic, LANGDeleteFilesOnTargetDisk,
    LANGSplitCaption, LANGCannotOpenFileS, LANGCannotSplitTheFileToMoreThan999Parts,
    LANGThereAreSomeFilesInTheTargetDirectorySDoYouWantToDeleteThem, LANGThereAreDFilesInTheTargetDirectoryDoYouWantToDeleteThem,
    LANGAnErrorOccuredWhileOperationS, LANGSplitOfSSucceeded, LANGSplitOfSFailed: string;

    LANGmnuShow_Caption, LANGmiShowDotFiles_Caption, LANGTheFileYouAreTryingToOpenIsQuiteBig,
    LANGCannotExecuteSPleaseCheckTheConfiguration, LANGEdit, LANGEnterFilenameToEdit: string;

    LANGmnuSettings_Caption, LANGmiFileTypes_Caption, LANGThereIsNoApplicationAssociatedWithS, LANGErrorExecutingCommand,
    LANGEditFileTypesCaption, LANGTitleLabel_Caption, LANGExtensionsColumn, LANGDescriptionColumn, LANGFileTypesList,
    LANGActionName, LANGCommand, LANGSetDefaultActionButton_Caption, LANGRunInTerminalCheckBox_Caption,
    LANGAutodetectCheckBox_Caption, LANGBrowseButton_Caption, LANGCommandLabel_Caption, LANGDescriptionLabel_Caption,
    LANGFNameExtLabel_Caption, LANGNotebookPageExtensions, LANGNotebookPageActions, LANGDefault,
    LANGCannotSaveFileTypeAssociationsBecauseOtherProcess: string;

    LANGDefaultColor, LANGIcon, LANGBrowseForIcon, LANGSelectFileTypeColor, LANGColor: string;

    LANGmiChangePermissions_Caption, LANGmiChangeOwner_Caption, LANGmiCreateSymlink_Caption, LANGmiEditSymlink_Caption,
    LANGChmodProgress, LANGChownProgress, LANGYouMustSelectAValidSymbolicLink, LANGPopupRunS, LANGPopupOpenS, LANGPopupGoUp,
    LANGPopupOpenWithS, LANGPopupDefault, LANGPopupOpenWith, LANGPopupViewFile, LANGPopupEditFile, LANGPopupMakeSymlink,
    LANGPopupRename, LANGPopupDelete, LANGDialogChangePermissions, LANGCouldNotBeChmoddedS, LANGDialogChangeOwner,
    LANGCouldNotBeChownedS, LANGDialogMakeSymlink, LANGDialogEditSymlink, LANGFEditSymlink_Caption,
    LANGFEditSymlink_SymbolicLinkFilename, LANGFEditSymlink_SymbolicLinkPointsTo: string;

    LANGFChmod_Caption, LANGFChmod_PermissionFrame, LANGFChmod_FileFrame, LANGFChmod_ApplyRecursivelyFor, LANGFChmod_miAllFiles,
    LANGFChmod_miDirectories, LANGFChmod_OctalLabel, LANGFChmod_SUID, LANGFChmod_SGID, LANGFChmod_Sticky, LANGFChmod_RUSR,
    LANGFChmod_WUSR, LANGFChmod_XUSR, LANGFChmod_RGRP, LANGFChmod_WGRP, LANGFChmod_XGRP, LANGFChmod_ROTH, LANGFChmod_WOTH,
    LANGFChmod_XOTH, LANGFChmod_TextLabel, LANGFChmod_FileLabel, LANGFChown_Caption, LANGFChown_OwnerFrame, LANGFChown_GroupFrame,
    LANGFChown_FileFrame, LANGFChown_ApplyRecursively, LANGFSymlink_Caption, LANGFSymlink_ExistingFilename,
    LANGFSymlink_SymlinkFilename: string;

    LANGmnuBookmarks_Caption, LANGmiAddBookmark_Caption, LANGmiEditBookmarks_Caption, LANGBookmarkPopupDelete_Caption,
    LANGmiPreferences_Caption, LANGTheCurrentDirectoryAlreadyExistsInTheBookmarksList, LANGSomeOtherInstanceChanged,
    LANGPreferences_Caption, LANGPreferences_TitleLabel_Caption, LANGPreferences_GeneralPage, LANGPreferences_FontsPage,
    LANGPreferences_ColorsPage, LANGPreferences_RowHeight, LANGPreferences_NumHistoryItems, LANGPreferences_Default,
    LANGPreferences_ClearReadonlyAttribute, LANGPreferences_DisableMouseRenaming, LANGPreferences_ShowFiletypeIconsInList,
    LANGPreferences_ExternalAppsLabel, LANGPreferences_Viewer, LANGPreferences_Editor, LANGPreferences_Terminal,
    LANGPreferences_ListFont, LANGPreferences_Change, LANGPreferences_UseDefaultFont, LANGPreferences_Foreground,
    LANGPreferences_Background, LANGPreferences_NormalItem, LANGPreferences_SetToDefaultToUseGTKThemeColors,
    LANGPreferences_Cursor, LANGPreferences_InactiveItem, LANGPreferences_SelectedItem, LANGPreferences_LinkItem,
    LANGPreferences_LinkItemHint, LANGPreferences_DotFileItem, LANGPreferences_DotFileItemHint,
    LANGPreferences_BrowseForApplication, LANGPreferences_DefaultS, LANGPreferences_SelectFont: string;

    LANGBookmarkButton_Tooltip, LANGUpButton_Tooltip, LANGRootButton_Tooltip, LANGHomeButton_Tooltip, LANGLeftEqualButton_Tooltip,
    LANGRightEqualButton_Tooltip, LANGmiShowDirectorySizes_Caption, LANGmiTargetSource_Caption, LANGFileTypeDirectory,
    LANGFileTypeFile, LANGFileTypeMetafile, LANGPreferencesPanelsPage, LANGPreferencesApplicationsPage,
    LANGPreferencesExperimentalPage, LANGPreferencesSelectAllDirectoriesCheckBox_Caption,
    LANGPreferencesNewStyleAltOCheckBox_Caption, LANGPreferencesNewStyleAltOCheckBox_Tooltip,
    LANGPreferencesShowFuncButtonsCheckBox_Caption, LANGPreferencesSizeFormatLabel_Caption, LANGPreferencesmiSizeFormat1,
    LANGPreferencesmiSizeFormat6, LANGPreferencesAutodetectXApp, LANGPreferencesAlwaysRunInTerminal,
    LANGPreferencesNeverRunInTerminal, LANGPreferencesCmdLineBehaviourLabel_Caption, LANGPreferencesFeatures,
    LANGPreferencesDisableMouseRename_Tooltip, LANGPreferencesDisableFileTipsCheckBox_Caption,
    LANGPreferencesDisableFileTipsCheckBox_Tooltip, LANGPreferencesShow, LANGPreferencesDirsInBoldCheckBox_Caption,
    LANGPreferencesDisableDirectoryBracketsCheckBox_Caption, LANGPreferencesOctalPermissionsCheckBox_Caption,
    LANGPreferencesOctalPermissionsCheckBox_Tooltip, LANGPreferencesMovement, LANGPreferencesLynxLikeMotionCheckBox_Caption,
    LANGPreferencesInsertMovesDownCheckBox_Caption, LANGPreferencesSpaceMovesDownCheckBox_Caption, LANGPreferencesViewer,
    LANGPreferencesCommandSC, LANGPreferencesUseInternalViewer, LANGPreferencesEditor, LANGPreferencesTerminal,
    LANGPreferencesExperimentalFeatures, LANGPreferencesExperimentalWarningLabel_Caption,
    LANGPreferencesFocusRefreshCheckBox_Caption, LANGPreferencesFocusRefreshCheckBox_Tooltip,
    LANGPreferencesWMCompatModeCheckBox_Caption, LANGPreferencesWMCompatModeCheckBox_Tooltip,
    LANGPreferencesCompatUseLibcSystemCheckBox_Caption, LANGPreferencesCompatUseLibcSystemCheckBox_Tooltip: string;

    LANGmiSearchCaption2, LANGmiNoMounterBarCaption, LANGmiShowOneMounterBarCaption, LANGmiShowTwoMounterBarCaption,
    LANGmnuNetworkCaption, LANGmiConnectionsCaption, LANGmiOpenConnectionCaption, LANGmiQuickConnectCaption, LANGmnuPluginsCaption,
    LANGmiTestPluginCaption, LANGmiMounterSettingsCaption, LANGmiColumnsCaption, LANGmiSavePositionCaption, LANGmiMountCaption,
    LANGmiUmountCaption, LANGmiEjectCaption, LANGmiDuplicateTabCaption, LANGmiCloseTabCaption, LANGmiCloseAllTabsCaption,
    LANGCannotDetermineDestinationEngine, LANGCannotLoadFile, LANGMountPointDevice, LANGMountSC, LANGNoPluginsFound,
    LANGPluginAbout, LANGCouldntOpenURI, LANGPluginAboutInside, LANGAreYouSureCloseAllTabs, LANGCouldntOpenURIArchive,
    LANGThereIsNoModuleAvailable, LANGIgnoreError, LANGErrorMount, LANGErrorUmount, LANGErrorEject: string;

    LANGMounterPrefs_Caption, LANGMounterPrefs_TitleLabelCaption, LANGMounterPrefs_ListViewFrameCaption,
    LANGMounterPrefs_MountName, LANGMounterPrefs_MountPoint, LANGMounterPrefs_Device, LANGMounterPrefs_MoveUpButtonTooltip,
    LANGMounterPrefs_MoveDownButtonTooltip, LANGMounterPrefs_UseFSTabDefaultsCheckBox,
    LANGMounterPrefs_UseFSTabDefaultsCheckBoxTooltip, LANGMounterPrefs_ToggleModeCheckBox,
    LANGMounterPrefs_ToggleModeCheckBoxTooltip, LANGMounterPrefs_PropertiesFrameCaption, LANGMounterPrefs_DisplayTextLabelCaption,
    LANGMounterPrefs_MountPointLabelCaption, LANGMounterPrefs_MountDeviceLabelCaption, LANGMounterPrefs_DeviceTypeLabelCaption,
    LANGMounterPrefs_miLocalDiskCaption, LANGMounterPrefs_miRemovableCaption, LANGMounterPrefs_miCDCaption,
    LANGMounterPrefs_miFloppyCaption, LANGMounterPrefs_miNetworkCaption, LANGMounterPrefs_MountCommandLabelCaption,
    LANGMounterPrefs_UmountCommandLabelCaption, LANGMounterPrefs_MountCommandEntryTooltip,
    LANGMounterPrefs_UmountCommandEntryTooltip, LANGMounterPrefs_IconLabelCaption: string;

    LANGConnMgr_Caption, LANGConnMgr_ConnectButton, LANGConnMgr_OpenConnection, LANGConnMgr_NameColumn, LANGConnMgr_URIColumn,
    LANGConnMgr_AddConnectionButtonCaption, LANGConnMgr_AddConnectionButtonTooltip, LANGConnMgr_EditButtonCaption,
    LANGConnMgr_EditButtonTooltip, LANGConnMgr_RemoveButtonCaption, LANGConnMgr_RemoveButtonTooltip,
    LANGConnMgr_DoYouWantDelete: string;

    LANGConnProp_FTP, LANGConnProp_SFTP, LANGConnProp_SMB, LANGConnProp_HTTP, LANGConnProp_HTTPS, LANGConnProp_Other,
    LANGConnProp_Caption, LANGConnProp_VFSModule, LANGConnProp_URI, LANGConnProp_URIEntryTooltip,
    LANGConnProp_DetailedInformations, LANGConnProp_Name, LANGConnProp_Server, LANGConnProp_Username,
    LANGConnProp_UserNameEntryTooltip, LANGConnProp_Password, LANGConnProp_TargetDirectory, LANGConnProp_ServiceType,
    LANGConnProp_MaskPassword, LANGConnProp_MenuItemCaption: string;

    LANGConnLogin_Caption, LANGConnLogin_Login, LANGConnLogin_ExperimentalWarningLabelCaption, LANGConnLogin_Username,
    LANGConnLogin_Password, LANGConnLogin_AnonymousCheckButton: string;

    LANGColumns_Caption, LANGColumns_Title, LANGColumns_MoveUpButtonTooltip, LANGColumns_MoveDownButtonTooltip,
    LANGColumns_TitlesLongName, LANGColumns_TitlesLongNameExt, LANGColumns_TitlesLongExt, LANGColumns_TitlesLongSize,
    LANGColumns_TitlesLongDateTime, LANGColumns_TitlesLongDate, LANGColumns_TitlesLongTime, LANGColumns_TitlesLongUser,
    LANGColumns_TitlesLongGroup, LANGColumns_TitlesLongAttr, LANGColumns_TitlesShortName, LANGColumns_TitlesShortNameExt,
    LANGColumns_TitlesShortExt, LANGColumns_TitlesShortSize, LANGColumns_TitlesShortDateTime, LANGColumns_TitlesShortDate,
    LANGColumns_TitlesShortTime, LANGColumns_TitlesShortUser, LANGColumns_TitlesShortGroup,
    LANGColumns_TitlesShortAttr: string;

    LANGTestPlugin_Caption, LANGTestPlugin_Title, LANGTestPlugin_ExperimentalWarningLabelCaption, LANGTestPlugin_Plugin,
    LANGTestPlugin_Command, LANGTestPlugin_Username, LANGTestPlugin_Password, LANGTestPlugin_AnonymousCheckButton,
    LANGTestPlugin_NoPluginsFound: string;

    LANGRemoteWait_Caption, LANGRemoteWait_OperationInProgress, LANGRemoteWait_ItemsFound: string;

    LANGSearch_Bytes, LANGSearch_kB, LANGSearch_MB, LANGSearch_days, LANGSearch_weeks, LANGSearch_months, LANGSearch_years,
    LANGSearch_Caption, LANGSearch_General, LANGSearch_Advanced, LANGSearch_SearchResults, LANGSearch_SearchFor,
    LANGSearch_FileMaskEntryTooltip, LANGSearch_SearchIn, LANGSearch_SearchArchivesCheckButton, LANGSearch_FindText,
    LANGSearch_FindTextEntryTooltip, LANGSearch_CaseSensitiveCheckButton, LANGSearch_StayCurrentFSCheckButton,
    LANGSearch_CaseSensitiveMatchCheckButton, LANGSearch_Size, LANGSearch_Date, LANGSearch_BiggerThan, LANGSearch_SmallerThan,
    LANGSearch_ModifiedBetweenRadioButton, LANGSearch_NotModifiedAfterRadioButton, LANGSearch_ModifiedLastRadioButton,
    LANGSearch_ModifiedNotLastRadionButton, LANGSearch_ModifiedBetweenEntry1, LANGSearch_ViewButtonCaption,
    LANGSearch_NewSearchButtonCaption, LANGSearch_GoToFileButtonCaption, LANGSearch_FeedToListboxButtonCaption,
    LANGSearch_StatusSC, LANGSearch_Ready, LANGSearch_PreparingToSearch, LANGSearch_SearchInProgress, LANGSearch_UserCancelled,
    LANGSearch_SearchFinished, LANGSearch_FilesFound, LANGSearch_And: string;

    LANGCloseOpenConnection, LANGDuplicateTabWarning, LANGDontShowAgain, LANGSwitchOtherPanelWarning, LANGOpenConnectionsWarning,
    LANGmiDisconnect_Caption, LANGDisconnectButton_Tooltip, LANGLeaveArchiveButton_Tooltip, LANGOpenTerminalButton_Tooltip,
    LANGOpenTerminalButton_Caption, LANGShowTextUIDsCheckBox_Caption, LANGShowTextUIDsCheckBox_Tooltip: string;
    
    LANGmiNewTab_Caption, LANGFilePopupMenu_Properties, LANGCommandEntry_Tooltip, LANGmiFiles_Caption: string;
    
    LANGPasswordButton_Tooltip, LANGHandleRunFromArchive_Bytes, LANGHandleRunFromArchive_FileTypeDesc_Unknown,
    LANGHandleRunFromArchive_NotAssociated, LANGHandleRunFromArchive_SelfExecutable,
    LANGHandleRunFromArchive_CouldntCreateTemporaryDirectory, LANGFRunFromVFS_Caption, LANGFRunFromVFS_TitleLabel,
    LANGFRunFromVFS_FileNameLabel, LANGFRunFromVFS_FileTypeLabel, LANGFRunFromVFS_SizeLabel, LANGFRunFromVFS_PackedSizeLabel,
    LANGFRunFromVFS_DateLabel, LANGFRunFromVFS_InfoLabel, LANGFRunFromVFS_OpensWithLabel, LANGFRunFromVFS_ExecuteButton,
    LANGFRunFromVFS_ExecuteAllButton, LANGFSetPassword_Caption, LANGFSetPassword_Label1_Caption, LANGFSetPassword_Label2_Caption,
    LANGFSetPassword_ShowPasswordCheckButton: string;
    
    LANGCopyFileNamesToClipboard, LANGCopyFullPathNamesToClipboard, LANGCopyPathToClipboard, LANGPreferences_DateFormatLabel_Caption,
    LANGPreferences_System, LANGPreferences_Custom, LANGPreferences_CustomDateFormatEntry_Tooltip, LANGPreferences_TimeFormatLabel_Caption,
    LANGPreferences_CustomTimeFormatEntry_Tooltip, LANGPreferences_DateTimeFormatLabel_Caption, LANGPreferences_QuickRenameSkipExtCheckBox,
    LANGPreferences_QuickRenameSkipExtCheckBox_Tooltip, LANGPreferences_SortDirectoriesLikeFilesCheckBox,
    LANGPreferences_SortDirectoriesLikeFilesCheckBox_Tooltip, LANGPreferences_QuickSearchLabel_Caption,
    LANGPreferences_QuickSearchOptionMenu_Tooltip, LANGPreferences_QuickSearch_Option1, LANGPreferences_QuickSearch_Option2,
    LANGPreferences_QuickSearch_Option3, LANGPreferences_QuickSearch_Option4, LANGPreferences_TempPathLabel_Caption,
    LANGPreferences_VFSTempPathLabel_Caption, LANGPreferences_VFSTempPathEntry_Tooltip: string;

    LANGPreferences_RightClickSelectCheckBox, LANGPreferences_RightClickSelectCheckBox_Tooltip,
    LANGGtkMountOperation_ConnectAnonymously, LANGGtkMountOperation_ConnectAsUser, LANGGtkMountOperation_Username,
    LANGGtkMountOperation_Domain, LANGGtkMountOperation_Password, LANGGtkMountOperation_DoNotSavePassword,
    LANGGtkMountOperation_ForgetPasswordImmediately, LANGGtkMountOperation_RememberPasswordUntilYouLogout,
    LANGGtkMountOperation_SavePasswordInConnectionManager, LANGGtkMountOperation_RememberForever, LANGFSymlink_RelativePath,
    LANGFConnectionManager_DuplicateButton_Caption, LANGFConnectionManager_DuplicateButton_Tooltip,
    LANGFConnectionManager_DoNotSavePasswordsCheckBox_Label, LANGFConnectionManager_DoNotSavePasswordsCheckBox_Tooltip,
    LANGFConnectionManager_DoNotSynchronizeKeyringCheckBox_Label, LANGFConnectionManager_DoNotSynchronizeKeyringCheckBox_Tooltip,
    LANGFConnectionManager_DuplicateMenuItem_Caption, LANGFQuickConnect_Caption, LANGFQuickConnect_TitleLabel_Caption,
    LANGFQuickConnect_ConnectToURILabel_Caption, LANGLinkToS, LANGOpenDirectoryInBackgroundTab, LANGTheActiveConnectionHasNotBeenSaved,
    LANGTheArchiveIsEncryptedAndRequiresPassword: string;







procedure AddTranslation(Language: string; LangSetProc: pointer);
procedure SetTranslationTexts(ForceLocale: string = '');

implementation

uses SysUtils, UTranslation_EN, UTranslation_CZ, UTranslation_RU, UTranslation_DE, UTranslation_SV, UTranslation_FR,
     UTranslation_ES, UTranslation_PL, UTranslation_UA, UTranslation_SR, UTranslation_HU, UTranslation_IT,
     UTranslation_CHT, UTranslation_CHS, UTranslation_SK, UTranslation_PT;



const LangTable: array of string = nil;
      LangProcTable: array of pointer = nil;

var Lang: string;

procedure AddTranslation(Language: string; LangSetProc: pointer);
begin
  if not Assigned(LangTable) then SetLength(LangTable, 0);
  if not Assigned(LangProcTable) then SetLength(LangProcTable, 0);
  SetLength(LangTable, Length(LangTable) + 1);
  LangTable[Length(LangTable) - 1] := ANSIUpperCase(Language);
  SetLength(LangProcTable, Length(LangProcTable) + 1);
  LangProcTable[Length(LangProcTable) - 1] := LangSetProc;
end;

function LookupLanguages: integer;
var i: integer;
begin
  Result := -1;
  for i := 0 to Length(LangTable) - 1 do
    if LangTable[i] = Lang then begin
      Result := i;
      Break;
    end;
end;

procedure SetTranslationTexts(ForceLocale: string = '');
var LangIdx: integer;
    SetProc: procedure;
begin
  if not (Assigned(LangTable) and (Length(LangTable) > 0)) then begin
    WriteLn('**** tuxcmd CRITICAL: No usable translations found. At least one is required. Probably you have compiled the application incorrectly.');
    Halt(1);
  end;
  //  Assign default language - English
  try
    Lang := 'EN';
    LangIdx := LookupLanguages;
    if LangIdx >= 0 then begin
      SetProc := LangProcTable[LangIdx];
      SetProc;
    end;
  except end;
  //  Assign other languages
  if Length(ForceLocale) = 0 then begin
    Lang := GetEnvironmentVariable('LC_MESSAGES');
    if Length(Trim(Lang)) = 0 then Lang := GetEnvironmentVariable('LC_ALL');
    if Length(Trim(Lang)) = 0 then Lang := GetEnvironmentVariable('LANGUAGE');
    if Length(Trim(Lang)) = 0 then Lang := GetEnvironmentVariable('LANG');
    if Length(Trim(Lang)) = 0 then Lang := 'en_US';   //  Default language
  end else Lang := ForceLocale;
  Lang := Trim(AnsiUpperCase(Lang));
  LangIdx := LookupLanguages;
  if (LangIdx = -1) and (Length(Lang) > 5) then begin
    Lang := Copy(Lang, 1, 5);
    LangIdx := LookupLanguages;
  end;  
  if (LangIdx = -1) and (Length(Lang) > 2) then begin
    Lang := Copy(Lang, 1, 2);
    LangIdx := LookupLanguages;
  end;  
  if LangIdx = -1 then begin
    Lang := 'EN';
    LangIdx := LookupLanguages;
  end;
  SetProc := LangProcTable[LangIdx];
  try
    SetProc;
  except
    on E: Exception do WriteLn('*** Exception ', E.ClassName, ' raised in SetProc - language ', LangTable[LangIdx], ', procedure @ ', integer(@SetProc), ': ', E.Message);
  end;
end;


end.
