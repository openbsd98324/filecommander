(*
    Tux Commander - UProgress - Progress dialog controlling various operations and related funcions 
    Copyright (C) 2004 Tomas Bzatek <tbzatek@users.sourceforge.net>
    Check for updates on tuxcmd.sourceforge.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
unit UProgress;

interface

uses
  SysUtils, Types, Classes, Variants, GTKControls, GTKForms, GTKStdCtrls, GTKExtCtrls, GTKConsts, GTKView,
  UEngines;

type
  TFProgress = class(TGTKDialog)
    Label1, Label2, Label3: TGTKLabel;
    {VBox,} Space, Space2, BarBox, LabelBox: TGTKVBox;
    HBox, Bar1HBox, Bar2HBox: TGTKHBox;
    ProgressBar, ProgressBar2: TGTKProgressBar;
    CancelButton: TGTKButton;
    procedure FormCreate(Sender: TObject); override;
    procedure FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CancelButtonClick(Sender: TObject);
  public
    Cancelled: boolean;
    FTwoBars: boolean;
    procedure SetNumBars(TwoBars: boolean);
  end;

var
  FProgress: TFProgress;

implementation

uses ULocale;


procedure TFProgress.FormCreate(Sender: TObject);
begin
  WindowPosition := wpCenter;
  Cancelled := False;
  Buttons := [];
  ShowSeparator := False;
  ActionArea.SetSizeRequest(0, 4);
  Caption := LANGProgress;
  CancelButton := TGTKButton.Create(Self);
  CancelButton.Caption := LANGCancel;
  CancelButton.SetSizeRequest(90, -1);
  Label1 := TGTKLabel.Create(Self);
  Label1.Caption := LANGDelete;
  Label2 := TGTKLabel.Create(Self);
  Label3 := TGTKLabel.Create(Self);
  Label2.Caption := LANGPreparingList;
  ProgressBar := TGTKProgressBar.Create(Self);
  ProgressBar.SetSizeRequest(420, 23);
  ProgressBar2 := TGTKProgressBar.Create(Self);
  ProgressBar2.SetSizeRequest(420, 23);
  Bar1HBox := TGTKHBox.Create(Self);
  Bar1HBox.AddControlEx(ProgressBar, False, False, 0);
  Bar2HBox := TGTKHBox.Create(Self);
  Bar2HBox.AddControlEx(ProgressBar2, False, False, 0);
  BarBox := TGTKVBox.Create(Self);
  HBox := TGTKHBox.Create(Self);
  HBox.AddControlEx(CancelButton, False, False, 0);
  Space := TGTKVBox.Create(Self);
  Space.SetSizeRequest(-1, 7);
  Space2 := TGTKVBox.Create(Self);
  Space2.SetSizeRequest(-1, 5);
  LabelBox := TGTKVBox.Create(Self);
//  VBox := TGTKVBox.Create(Self);
  ClientArea.AddControlEx(LabelBox, True, True, 5);
  ClientArea.AddControlEx(BarBox, False, False, 10);
  ClientArea.AddControlEx(HBox, False, False, 0);
//  ClientArea.AddControlEx(Space, False, False, 0);
//  ClientArea.AddControlEx(VBox, True, True, 0);
//  AddControl(VBox);
  OnKeyDown := FormKeyDown;
  OnCloseQuery := FormCloseQuery;
  CancelButton.OnClick := CancelButtonClick;
  CancelButton.Default := True;
end;

procedure TFProgress.FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
begin
  if Key = GDK_ESCAPE then begin
    Accept := False;
    CancelButtonClick(Sender);
  end;
end;

procedure TFProgress.CancelButtonClick(Sender: TObject);
begin
  Cancelled := True;
end;

procedure TFProgress.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CancelButtonClick(Sender);
  CanClose := False;
end;

procedure TFProgress.SetNumBars(TwoBars: boolean);
begin
  FTwoBars := TwoBars;
  if not TwoBars then begin
    LabelBox.AddControlEx(Label1, True, True, 5);
    LabelBox.AddControlEx(Label2, True, True, 0);
    BarBox.AddControlEx(Bar1HBox, False, False, 0);
    SetDefaultSize(480, -1);
    SetSizeRequest(480, -1);
  end else begin
    LabelBox.AddControlEx(Label1, True, True, 0);
    LabelBox.AddControlEx(Label2, True, True, 0);
    LabelBox.AddControlEx(Label3, True, True, 0);
    Label2.XAlign := 0;   Label2.XPadding := 20;
    Label3.XAlign := 0;   Label3.XPadding := 20;
    BarBox.AddControlEx(Bar1HBox, False, False, 0);
    BarBox.AddControlEx(Space2, False, False, 0);
    BarBox.AddControlEx(Bar2HBox, False, False, 0);
    SetDefaultSize(480, -1);
    SetSizeRequest(480, -1);
  end;
end;


end.

