(*
    Tux Commander - UChown - Change owner dialog
    Copyright (C) 2004 Tomas Bzatek <tbzatek@users.sourceforge.net>
    Check for updates on tuxcmd.sourceforge.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
unit UChown;

interface

uses
  SysUtils, Classes, GTKControls, GTKForms, GTKStdCtrls, GTKExtCtrls, GTKConsts, GTKView;

type
  TFChown = class(TGTKDialog)
    HBox, HBox2: TGTKHBox;
    VBox: TGTKVBox;
    OwnerFrame, GroupFrame, FileFrame: TGTKFrame;
    OwnerListView, GroupListView: TGTKListView;
    OwnerListViewScrolledWindow, GroupListViewScrolledWindow: TGTKScrolledWindow;
    RecursiveCheckButton: TGTKCheckButton;
    FileLabel: TGTKLabel;
    procedure FormCreate(Sender: TObject); override;
    procedure FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
    procedure OwnerListViewSelectionChanged(Sender: TObject);
    procedure GroupListViewSelectionChanged(Sender: TObject);
  public
    LastUID, LastGID: integer;
    procedure AssignMode(const Mode: Cardinal; const FileName: string; const UID, GID: integer);
  private
    procedure FillData;
  end;

var
  FChown: TFChown;

implementation

uses ULibc, ULocale, UCoreUtils, UCoreClasses;


procedure TFChown.FormCreate(Sender: TObject);
begin
  SetDefaultSize(-1, -1);
  LastUID := geteuid;
  LastGID := getegid;
  Caption := LANGFChown_Caption;
  Buttons := [mbOK, mbCancel];
  HBox := TGTKHBox.Create(Self);
  HBox.Homogeneous := False;
  HBox.BorderWidth := 6;
  OwnerFrame := TGTKFrame.Create(Self);
  OwnerFrame.Caption := LANGFChown_OwnerFrame;
  GroupFrame := TGTKFrame.Create(Self);
  GroupFrame.Caption := LANGFChown_GroupFrame;
  FileFrame := TGTKFrame.Create(Self);
  FileFrame.Caption := LANGFChown_FileFrame;
  HBox.AddControlEx(OwnerFrame, True, True, 5);
  HBox.AddControlEx(GroupFrame, True, True, 5);
  HBox.AddControlEx(FileFrame, True, True, 5);
  HBox2 := TGTKHBox.Create(Self);
  HBox2.Homogeneous := False;
  RecursiveCheckButton := TGTKCheckButton.CreateWithLabel(Self, LANGFChown_ApplyRecursively);
  HBox2.AddControlEx(RecursiveCheckButton, False, False, 15);

  ClientArea.AddControlEx(HBox, True, True, 0);
  ClientArea.AddControlEx(TGTKHSeparator.Create(Self), False, False, 5);
  ClientArea.AddControlEx(HBox2, False, False, 3);

  FileLabel := TGTKLabel.Create(Self);
  FileLabel.SetAlignment(0, 0);
  FileLabel.SetPadding(10, 5);
  FileLabel.Caption := '<span weight="ultrabold">File:</span> .adobe'#10'<span weight="ultrabold">Text:</span> rw-rw-rw'#10 +
                       '<span weight="ultrabold">Octal:</span> 666'#10'<span weight="ultrabold">Owner:</span> root'#10 +
                       '<span weight="ultrabold">Group:</span> root';                       
  FileLabel.UseMarkup := True;
  FileFrame.AddControl(FileLabel);

  OwnerListView := TGTKListView.CreateTyped(Self, False, [lcText, lcNumber]);
  OwnerListView.RulesHint := True;
  OwnerListView.ShowHeaders := False;
  OwnerListView.Columns.Add.AddAttribute('text', 0);
  OwnerListViewScrolledWindow := TGTKScrolledWindow.Create(Self);
  OwnerListViewScrolledWindow.ShadowType := stShadowIn;
  OwnerListViewScrolledWindow.HorizScrollBarPolicy := sbAutomatic;
  OwnerListViewScrolledWindow.VertScrollBarPolicy := sbAutomatic;
  OwnerListViewScrolledWindow.BorderWidth := 10;
  OwnerListViewScrolledWindow.AddControl(OwnerListView);
  OwnerListViewScrolledWindow.SetSizeRequest(200, 200);
  OwnerFrame.AddControl(OwnerListViewScrolledWindow);

  GroupListView := TGTKListView.CreateTyped(Self, False, [lcText, lcNumber]);
  GroupListView.RulesHint := True;
  GroupListView.ShowHeaders := False;
  GroupListView.Columns.Add.AddAttribute('text', 0);
  GroupListViewScrolledWindow := TGTKScrolledWindow.Create(Self);
  GroupListViewScrolledWindow.ShadowType := stShadowIn;
  GroupListViewScrolledWindow.HorizScrollBarPolicy := sbAutomatic;
  GroupListViewScrolledWindow.VertScrollBarPolicy := sbAutomatic;
  GroupListViewScrolledWindow.BorderWidth := 10;
  GroupListViewScrolledWindow.AddControl(GroupListView);
  GroupListViewScrolledWindow.SetSizeRequest(200, 200);
  GroupFrame.AddControl(GroupListViewScrolledWindow);

  FillData;
  OnKeyDown := FormKeyDown;
end;

procedure TFChown.FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
begin
  case Key of
    GDK_RETURN, GDK_KP_ENTER: ModalResult := mbOK;
    GDK_ESCAPE: ModalResult := mbCancel;
  end;
end;

procedure TFChown.AssignMode(const Mode: Cardinal; const FileName: string; const UID, GID: integer);
var i: integer;
    susr, sgrp: string;
begin
  LastUID := UID;
  LastGID := GID;
  susr := 'N/A';
  sgrp := 'N/A';
  //  Lookup and select current user and group
  if OwnerListView.Items.Count > 0 then
    for i := 0 to OwnerListView.Items.Count - 1 do
      if OwnerListView.Items[i].AsInteger(1) = UID then begin
        OwnerListView.Items[i].Selected := True;
        OwnerListView.Items[i].SetCursor(0, False, not Application.GTKVersion_2_2_0_Up, 0.5, 0);
        susr := OwnerListView.Items[i].AsString(0);
        Break;
      end;
  if GroupListView.Items.Count > 0 then
    for i := 0 to GroupListView.Items.Count - 1 do
      if GroupListView.Items[i].AsInteger(1) = GID then begin
        GroupListView.Items[i].Selected := True;
        GroupListView.Items[i].SetCursor(0, False, not Application.GTKVersion_2_2_0_Up, 0.5, 0);
        sgrp := GroupListView.Items[i].AsString(0);
        Break;
      end;
  //  Fill more info
  FileLabel.Caption := Format(LANGFChmod_FileLabel, [StrToUTF8(FileName), AttrToStr(Mode), AttrToOctal(Mode), susr, sgrp]);
  FileLabel.UseMarkup := True;
  if Length(FileName) > 20 then FileLabel.SetSizeRequest(200, -1);
  OwnerListView.OnSelectionChanged := OwnerListViewSelectionChanged;
  GroupListView.OnSelectionChanged := GroupListViewSelectionChanged;
end;

procedure TFChown.FillData;
var UsrManager: TUserManager;
    i: integer;
    Item: TGTKListItem;
begin
  UsrManager := TUserManager.Create;
  try
    if UsrManager.UserList.Count > 0 then
      for i := 0 to UsrManager.UserList.Count - 1 do begin
        Item := OwnerListView.Items.Add;
        Item.SetValue(0, TSystemUser(UsrManager.UserList[i]).UserName);
        Item.SetValue(1, TSystemUser(UsrManager.UserList[i]).UID);
      end;
    if UsrManager.GroupList.Count > 0 then
      for i := 0 to UsrManager.GroupList.Count - 1 do begin
        Item := GroupListView.Items.Add;
        Item.SetValue(0, TSystemGroup(UsrManager.GroupList[i]).GroupName);
        Item.SetValue(1, TSystemGroup(UsrManager.GroupList[i]).GID);
      end;
  finally
    UsrManager.Free;
  end;
end;

procedure TFChown.OwnerListViewSelectionChanged(Sender: TObject);
begin
  if Assigned(OwnerListView.Selected) then LastUID := OwnerListView.Selected.AsInteger(1);
end;

procedure TFChown.GroupListViewSelectionChanged(Sender: TObject);
begin
  if Assigned(GroupListView.Selected) then LastGID := GroupListView.Selected.AsInteger(1);
end;

end.

