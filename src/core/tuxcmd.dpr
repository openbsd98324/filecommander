(*
    Tux Commander - an open-source file manager with 2 panels side by side written for GTK2
    Copyright (C) 2007 by Tomas Bzatek <tbzatek@users.sourceforge.net>
    Check for updates on tuxcmd.sourceforge.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)

program tuxcmd;

uses
  {$IFDEF FPC}
  cthreads,
  cwstring,
  {$ENDIF}
  GTKForms,
  UConfig in 'UConfig.pas',
  UGnome in 'UGnome.pas',
  UMain in 'UMain.pas' {FMain},
  UCore in 'UCore.pas',
  UEngines in 'UEngines.pas',
  USelect in 'USelect.pas' {FSelect},
  URemoteWait in 'URemoteWait.pas' {FRemoteWait},
  UDirDelete in 'UDirDelete.pas' {FDirDelete},
  UProgress in 'UProgress.pas' {FProgress},
  UCopyMove in 'UCopyMove.pas' {FCopyMove},
  UOverwrite in 'UOverwrite.pas' {FOverwrite},
  UCoreUtils in 'UCoreUtils.pas',
  ULocale in 'ULocale.pas',
  UChecksum in 'UChecksum.pas' {FChecksum},
  UChecksumDruid in 'UChecksumDruid.pas' {FChecksumDruid},
  USplitFile in 'USplitFile.pas' {FSplitFile},
  UFileAssoc in 'UFileAssoc.pas',
  UFileTypeSettings in 'UFileTypeSettings.pas' {FFileTypeSettings},
  UChmod in 'UChmod.pas' {FChmod},
  UChown in 'UChown.pas' {FChown},
  UCoreClasses in 'UCoreClasses.pas',
  USymlink in 'USymlink.pas' {FSymlink},
  UPreferences in 'UPreferences.pas' {FPreferences},
  UViewer in 'UViewer.pas' {FViewer},
  UTestPlugin in 'UTestPlugin.pas',
  UMounterPrefs in 'UMounterPrefs.pas',
  UColumns in 'UColumns.pas',
  UVFSCore in 'vfs/UVFSCore.pas',
  uVFSprototypes in 'vfs/uVFSprototypes.pas',
  UToolTips in 'UToolTips.pas',
  UConnectionManager in 'UConnectionManager.pas',
  UConnectionProperties in 'UConnectionProperties.pas',
  USearch in 'USearch.pas',
  UGlibThreads in 'UGlibThreads.pas',
  URunFromVFS in 'URunFromVFS.pas',
  ULibc in 'ULibc.pas',
  UQuickConnect in 'UQuickConnect.pas',
  UTranslation_EN in 'translations/UTranslation_EN.pas',
  UTranslation_CZ in 'translations/UTranslation_CZ.pas',
  UTranslation_RU in 'translations/UTranslation_RU.pas',
  UTranslation_DE in 'translations/UTranslation_DE.pas',
  UTranslation_SV in 'translations/UTranslation_SV.pas',
  UTranslation_FR in 'translations/UTranslation_FR.pas',
  UTranslation_ES in 'translations/UTranslation_ES.pas',
  UTranslation_PL in 'translations/UTranslation_PL.pas',
  UTranslation_UA in 'translations/UTranslation_UA.pas',
  UTranslation_SR in 'translations/UTranslation_SR.pas',
  UTranslation_HU in 'translations/UTranslation_HU.pas',
  UTranslation_IT in 'translations/UTranslation_IT.pas',
  UTranslation_CHT in 'translations/UTranslation_CHT.pas',
  UTranslation_CHS in 'translations/UTranslation_CHS.pas',
  UTranslation_SK in 'translations/UTranslation_SK.pas',
  UTranslation_PT in 'translations/UTranslation_PT.pas';


{$IFNDEF FPC}
  {$R *.res}
{$ENDIF}

begin
  Application.Initialize;
  Application.CreateForm(TFMain, FMain);
  Application.Run;
end.
