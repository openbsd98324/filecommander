(*
    Tux Commander - URunFromVFS - The file properties dialog for VFS filesystems
    Copyright (C) 2007 Tomas Bzatek <tbzatek@users.sourceforge.net>
    Check for updates on tuxcmd.sourceforge.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
unit URunFromVFS;

interface

uses
  glib2, gtk2, pango, SysUtils, Types, Classes, GTKControls, GTKForms, GTKStdCtrls, GTKExtCtrls, GTKConsts, GTKView,
  GTKUtils, GTKDialogs, GTKPixbuf, GTKClasses, UCoreClasses;

type
  TFRunFromVFS = class(TGTKDialog)
    TitleFrame, ListFontFrame: TGTKFrame;
    TitleLabel: TGTKLabel;
    TitleEventBox: TGTKEventBox;
    TitleIcon: TGTKImage;
    TitleHBox: TGTKHBox;
    ButtonBox: TGTKHButtonBox;
    CloseButton: TGTKButton;

    Table: TGTKTable;
    ExecuteButton, ExecuteAllButton: TGTKImageButton;
    
    FileNameLabel, FileNameLabel2, FileTypeLabel, FileTypeLabel2, SizeLabel, SizeLabel2, PackedSizeLabel, PackedSizeLabel2,
    DateLabel, DateLabel2: TGTKLabel;
    OpensWithLabel, OpensWithLabel2, InfoLabel: TGTKLabel;
    InfoIcon: TGTKImage;
    
    HBox1, HBox2: TGTKHBox;
    procedure FormCreate(Sender: TObject); override;
    procedure FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
    procedure ExecuteButtonClick(Sender: TObject);
    procedure ExecuteAllButtonClick(Sender: TObject);
    procedure CloseButtonClick(Sender: TObject);
  end;

var
  FRunFromVFS: TFRunFromVFS;

implementation

uses ULocale, UConfig;


procedure TFRunFromVFS.FormCreate(Sender: TObject);
begin
//  SetDefaultSize(55, 40);
  Caption := LANGFRunFromVFS_Caption;
  Buttons := [];
  ShowSeparator := False;
  TitleEventBox := TGTKEventBox.Create(Self);
  TitleLabel := TGTKLabel.Create(Self);
  TitleLabel.Caption := Format('<span size="x-large" weight="ultrabold">%s</span>', [LANGFRunFromVFS_TitleLabel]);
  TitleLabel.UseMarkup := True;
  TitleLabel.XAlign := 0;
  TitleLabel.XPadding := 0;
  TitleLabel.YPadding := 3;
  TitleEventBox.ControlState := csPrelight;
  TitleFrame := TGTKFrame.CreateWithoutLabel(Self);
  TitleFrame.ShadowType := stShadowOut;
  TitleIcon := TGTKImage.Create(Self);
  TitleIcon.SetFromStock('gtk-properties', isLargeToolbar);
  TitleHBox := TGTKHBox.Create(Self);
  TitleHBox.Homogeneous := False;
  TitleHBox.AddControlEx(TGTKVBox.Create(Self), False, False, 5);
  TitleHBox.AddControlEx(TitleIcon, False, False, 0);
  TitleHBox.AddControlEx(TitleLabel, True, True, 10);
  TitleEventBox.AddControl(TitleHBox);
  TitleFrame.AddControl(TitleEventBox);
  ClientArea.AddControlEx(TitleFrame, False, True, 0);

  CloseButton := TGTKButton.CreateFromStock(Self, GTK_STOCK_CLOSE);
//  CloseButton.Default := True;
//  Default := CloseButton;
  CloseButton.OnClick := CloseButtonClick;
  ButtonBox := TGTKHButtonBox.Create(Self);
  ButtonBox.Layout := blEnd;
  ButtonBox.Spacing := 0;
  ButtonBox.BorderWidth := 0;

  FileNameLabel := TGTKLabel.Create(Self);
  FileNameLabel.XAlign := 1;
  FileNameLabel.YAlign := 0;
  FileNameLabel.Caption := Format('<span weight="ultrabold">%s</span>', [LANGFRunFromVFS_FileNameLabel]);
  FileNameLabel.UseMarkup := True;
  FileNameLabel2 := TGTKLabel.Create(Self);
  FileNameLabel2.Caption := Format('%s<span weight="ultrabold"> </span>', ['/tmp/t/bbbbb.txt']);
  FileNameLabel2.XAlign := 0;
  FileNameLabel2.UseMarkup := True;
  FileNameLabel2.Selectable := True;
  FileNameLabel2.CanFocus := False;
  FileNameLabel2.LineWrap := True;

  FileTypeLabel := TGTKLabel.Create(Self);
  FileTypeLabel.XAlign := 1;
  FileTypeLabel.Caption := Format('<span weight="ultrabold">%s</span>', [LANGFRunFromVFS_FileTypeLabel]);
  FileTypeLabel.UseMarkup := True;
  FileTypeLabel2 := TGTKLabel.Create(Self);
  FileTypeLabel2.Caption := Format('%s<span weight="ultrabold"> </span>', ['HTML page']);
  FileTypeLabel2.XAlign := 0;
  FileTypeLabel2.UseMarkup := True;

  SizeLabel := TGTKLabel.Create(Self);
  SizeLabel.XAlign := 1;
  SizeLabel.Caption := Format('<span weight="ultrabold">%s</span>', [LANGFRunFromVFS_SizeLabel]);
  SizeLabel.UseMarkup := True;
  SizeLabel2 := TGTKLabel.Create(Self);
  SizeLabel2.Caption := Format('%s<span weight="ultrabold"> </span>', ['192 168 bytes']);
  SizeLabel2.XAlign := 0;
  SizeLabel2.UseMarkup := True;

  PackedSizeLabel := TGTKLabel.Create(Self);
  PackedSizeLabel.XAlign := 1;
  PackedSizeLabel.Caption := Format('<span weight="ultrabold">%s</span>', [LANGFRunFromVFS_PackedSizeLabel]);
  PackedSizeLabel.UseMarkup := True;
  PackedSizeLabel2 := TGTKLabel.Create(Self);
  PackedSizeLabel2.Caption := Format('%s<span weight="ultrabold"> </span>', ['168 192 bytes']);
  PackedSizeLabel2.XAlign := 0;
  PackedSizeLabel2.UseMarkup := True;

  DateLabel := TGTKLabel.Create(Self);
  DateLabel.XAlign := 1;
  DateLabel.Caption := Format('<span weight="ultrabold">%s</span>', [LANGFRunFromVFS_DateLabel]);
  DateLabel.UseMarkup := True;
  DateLabel2 := TGTKLabel.Create(Self);
  DateLabel2.Caption := Format('%s<span weight="ultrabold"> </span>', ['2007-01-01']);
  DateLabel2.XAlign := 0;
  DateLabel2.UseMarkup := True;

  HBox1 := TGTKHBox.Create(Self);
  HBox1.Homogeneous := False;
  InfoLabel := TGTKLabel.Create(Self);
  InfoLabel.Caption := LANGFRunFromVFS_InfoLabel;
  InfoLabel.UseMarkup := True;
  InfoLabel.LineWrap := True;
  InfoLabel.SetSizeRequest(400, -1);
  InfoIcon := TGTKImage.Create(Self);
  InfoIcon.SetFromStock('gtk-dialog-info', isDialog);
  HBox1.AddControlEx(TGTKVBox.Create(Self), False, False, 12);
  HBox1.AddControlEx(InfoIcon, False, False, 7);
  HBox1.AddControlEx(InfoLabel, True, True, 0);
  HBox1.AddControlEx(TGTKVBox.Create(Self), False, False, 8);

  OpensWithLabel := TGTKLabel.Create(Self);
  OpensWithLabel.XAlign := 1;
  OpensWithLabel.YAlign := 0;
  OpensWithLabel.Caption := Format('<span weight="ultrabold">%s</span>', [LANGFRunFromVFS_OpensWithLabel]);
  OpensWithLabel.UseMarkup := True;
  OpensWithLabel2 := TGTKLabel.Create(Self);
  OpensWithLabel2.Caption := Format('%s<span weight="ultrabold"> </span>', ['galeon']);
  OpensWithLabel2.XAlign := 0;
  OpensWithLabel2.Selectable := True;
  OpensWithLabel2.CanFocus := False;
  OpensWithLabel2.UseMarkup := True;

  ExecuteButton := TGTKImageButton.Create(Self);
  ExecuteButton.SetFromStock('gtk-execute', isButton);
  ExecuteButton.Caption := LANGFRunFromVFS_ExecuteButton;
  ExecuteButton.OnClick := ExecuteButtonClick;
  ExecuteButton.Spacing := 7;
  ExecuteAllButton := TGTKImageButton.Create(Self);
  ExecuteAllButton.Caption := LANGFRunFromVFS_ExecuteAllButton;
  ExecuteAllButton.OnClick := ExecuteAllButtonClick;
  ExecuteAllButton.Spacing := 7;


  Table := TGTKTable.Create(Self);
  Table.SetRowColCount(13, 4);
  ClientArea.AddControlEx(Table, True, True, 12);
  Table.AddControlEx(0, 0, 1, 1, TGTKVBox.Create(Self), [taoShrink], [taoShrink], 10, 6);
  Table.AddControlEx(0, 1, 1, 1, TGTKVBox.Create(Self), [taoShrink], [taoShrink, taoFill], 12, 2);
  Table.AddControlEx(1, 1, 1, 1, FileNameLabel, [taoShrink, taoFill], [taoShrink, taoFill], 10, 2);
  Table.AddControlEx(3, 1, 2, 1, FileNameLabel2, [taoExpand, taoFill], [taoShrink, taoFill], 10, 2);
  Table.AddControlEx(1, 2, 1, 1, FileTypeLabel, [taoShrink, taoFill], [taoShrink, taoFill], 10, 2);
  Table.AddControlEx(3, 2, 2, 1, FileTypeLabel2, [taoExpand, taoFill], [taoShrink, taoFill], 10, 2);
  Table.AddControlEx(1, 3, 1, 1, SizeLabel, [taoShrink, taoFill], [taoShrink, taoFill], 10, 2);
  Table.AddControlEx(3, 3, 2, 1, SizeLabel2, [taoExpand, taoFill], [taoShrink, taoFill], 10, 2);
  Table.AddControlEx(1, 4, 1, 1, PackedSizeLabel, [taoShrink, taoFill], [taoShrink, taoFill], 10, 2);
  Table.AddControlEx(3, 4, 2, 1, PackedSizeLabel2, [taoExpand, taoFill], [taoShrink, taoFill], 10, 2);
  Table.AddControlEx(1, 5, 1, 1, DateLabel, [taoShrink, taoFill], [taoShrink, taoFill], 10, 2);
  Table.AddControlEx(3, 5, 2, 1, DateLabel2, [taoExpand, taoFill], [taoShrink, taoFill], 10, 2);
  Table.AddControlEx(0, 6, 5, 1, TGTKHBox.Create(Self), [taoShrink], [taoShrink], 50, 17);
  Table.AddControlEx(0, 7, 5, 1, HBox1, [taoExpand, taoFill], [taoShrink], 0, 2);
  Table.AddControlEx(1, 8, 1, 1, OpensWithLabel, [taoShrink, taoFill], [taoShrink, taoFill], 10, 10);
  Table.AddControlEx(3, 8, 2, 1, OpensWithLabel2, [taoExpand, taoFill], [taoShrink, taoFill], 10, 10);
//  Table.AddControlEx(3, 9, 1, 1, ExecuteButton, [taoFill], [taoShrink], 10, 2);
//  Table.AddControlEx(3, 10, 1, 1, ExecuteAllButton, [taoFill], [taoShrink], 10, 2);
  Table.AddControlEx(4, 11, 1, 1, TGTKHBox.Create(Self), [taoExpand, taoFill], [taoShrink], 30, 2);
  Table.AddControlEx(0, 12, 4, 1, TGTKVBox.Create(Self), [taoExpand, taoFill], [taoShrink], 30, 10);

  HBox2 := TGTKHBox.Create(Self);
  HBox2.BorderWidth := 3;
  HBox2.Homogeneous := False;
  HBox2.AddControlEx(ExecuteButton, False, False, 2);
  HBox2.AddControlEx(ExecuteAllButton, False, False, 2);
  HBox2.AddControlEx(TGTKEventBox.Create(Self), False, False, 4);
  ButtonBox.AddControlEnd(CloseButton);
  HBox2.AddControlEx(ButtonBox, False, False, 2);
  ActionArea.AddControlEx(HBox2, False, False, 0);

  CloseButton.SetFocus;
  OnKeyDown := FormKeyDown;
end;

procedure TFRunFromVFS.FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
begin
  if Key = GDK_ESCAPE then begin
    ModalResult := mbClose;
    Accept := False;
  end else
  if Key = GDK_RIGHT then begin
    if ExecuteButton.Focused then ExecuteAllButton.SetFocus else
    if ExecuteAllButton.Focused then CloseButton.SetFocus else
    ExecuteButton.SetFocus;
    Accept := False;
  end else
  if Key = GDK_LEFT then begin
    if ExecuteButton.Focused then CloseButton.SetFocus else
    if ExecuteAllButton.Focused then ExecuteButton.SetFocus else
    ExecuteAllButton.SetFocus;
    Accept := False;
  end;
end;

procedure TFRunFromVFS.ExecuteButtonClick(Sender: TObject);
begin
  ModalResult := mbYes;
end;

procedure TFRunFromVFS.ExecuteAllButtonClick(Sender: TObject);
begin
  ModalResult := mbNo;
end;

procedure TFRunFromVFS.CloseButtonClick(Sender: TObject);
begin
  ModalResult := mbClose;
end;


end.

