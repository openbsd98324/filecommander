(*
    Tux Commander - UColumns - Panel Columns Settings dialog
    Copyright (C) 2007 Tomas Bzatek <tbzatek@users.sourceforge.net>
    Check for updates on tuxcmd.sourceforge.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
unit UColumns;

interface

uses
  glib2, gdk2, gtk2, pango, SysUtils, Types, Classes, Variants, GTKControls, GTKForms, GTKStdCtrls, GTKExtCtrls, GTKConsts, GTKView,
  GTKUtils, GTKDialogs, GTKPixbuf, GTKClasses,     
  UCoreClasses;

type
  TFColumns = class(TGTKDialog)
    TitleFrame: TGTKFrame;
    TitleLabel: TGTKLabel;
    TitleEventBox: TGTKEventBox;
    TitleIcon: TGTKImage;
    TitleHBox: TGTKHBox;
    ListView: TGTKListView;
    ListViewScrolledWindow: TGTKScrolledWindow;
    ListViewTable: TGTKTable;
    MoveUpButton, MoveDownButton: TGTKImageButton;
    procedure FormCreate(Sender: TObject); override;
    procedure ListViewSelectionChanged(Sender: TObject);
    procedure MoveUpDownButtonButtonClick(Sender: TObject);
    procedure ListViewColumnToggled(Sender: TObject; Column: TGTKTreeViewColumn; Item: TGTKListItem);
  private
    procedure AddColumnItems;
  public
    procedure ApplyColumnList;
  end;

var
  FColumns: TFColumns;

implementation

uses ULocale, UCoreUtils, UConfig, UCore;


procedure TFColumns.FormCreate(Sender: TObject);
var Column: TGTKTreeViewColumn;
    row_targets: TGtkTargetEntry;
begin
  row_targets.target := PChar('GTK_TREE_MODEL_ROW');
  row_targets.flags := GTK_TARGET_SAME_WIDGET;
  row_targets.info := 1;

  SetDefaultSize(250, 350);
  Caption := LANGColumns_Caption;
  Buttons := [mbOK, mbCancel];
  ShowSeparator := False;
  TitleEventBox := TGTKEventBox.Create(Self);
  TitleLabel := TGTKLabel.Create(Self);
  TitleLabel.Caption := Format('<span size="x-large" weight="ultrabold">%s</span>', [LANGColumns_Title]);
  TitleLabel.UseMarkup := True;
  TitleLabel.XAlign := 0;
  TitleLabel.XPadding := 0;
  TitleLabel.YPadding := 3;
  TitleEventBox.ControlState := csPrelight;
  TitleFrame := TGTKFrame.CreateWithoutLabel(Self);
  TitleFrame.ShadowType := stShadowOut;
  TitleIcon := TGTKImage.Create(Self);
  TitleIcon.SetFromStock('gtk-justify-center', isLargeToolbar);
  TitleHBox := TGTKHBox.Create(Self);
  TitleHBox.Homogeneous := False;
  TitleHBox.AddControlEx(TGTKEventBox.Create(Self), False, False, 5);
  TitleHBox.AddControlEx(TitleIcon, False, False, 0);
  TitleHBox.AddControlEx(TitleLabel, True, True, 10);
  TitleEventBox.AddControl(TitleHBox);
  TitleFrame.AddControl(TitleEventBox);
  ClientArea.AddControlEx(TitleFrame, False, True, 0);

  ListViewTable := TGTKTable.Create(Self);
  ListViewTable.BorderWidth := 7;
  ClientArea.AddControlEx(ListViewTable, True, True, 0);

  ListView := TGTKListView.CreateTyped(Self, False, [lcBoolean, lcText, lcNumber]);

  {  ListView.Reorderable := True;
  g_object_set(ListView.FWidget, 'reorderable', integer(True), nil); }
  gtk_tree_view_enable_model_drag_source(GTK_TREE_VIEW(ListView.FWidget), GDK_BUTTON1_MASK, @row_targets, 1, GDK_ACTION_MOVE);
  gtk_tree_view_enable_model_drag_dest(GTK_TREE_VIEW(ListView.FWidget), @row_targets, 1, GDK_ACTION_MOVE);

  ListView.RulesHint := True;
  ListView.ShowHeaders := False;
  Column := ListView.Columns.AddTyped(ctToggle);
  Column.AddAttribute('active', 0);
  Column.Resizable := False;
  Column.FixedWidth := 20;
  Column.SizingMode := smFixed;
  Column.OnToggled := ListViewColumnToggled;
  Column := ListView.Columns.Add;
  Column.AddAttribute('text', 1);
  Column.Resizable := False;
  Column.SizingMode := smAutoSize;
  ListViewScrolledWindow := TGTKScrolledWindow.Create(Self);
  ListViewScrolledWindow.AddControl(ListView);
  ListViewScrolledWindow.HorizScrollBarPolicy := sbAutomatic;
  ListViewScrolledWindow.VertScrollBarPolicy := sbAutomatic;
  ListViewScrolledWindow.ShadowType := stShadowIn;

  MoveUpButton := TGTKImageButton.CreateWithoutLabel(Self);
  MoveUpButton.SetFromStock('gtk-go-up', isSmallToolbar);
  MoveUpButton.Tooltip := LANGColumns_MoveUpButtonTooltip;
  MoveUpButton.CanFocus := False;
  MoveDownButton := TGTKImageButton.CreateWithoutLabel(Self);
  MoveDownButton.SetFromStock('gtk-go-down', isSmallToolbar);
  MoveDownButton.Tooltip := LANGColumns_MoveDownButtonTooltip;
  MoveDownButton.CanFocus := False;

  ListViewTable.AddControlEx(0, 1, 3, 4, ListViewScrolledWindow, [taoExpand, taoFill], [taoExpand, taoFill], 0, 5);
  ListViewTable.AddControlEx(3, 2, 1, 1, MoveUpButton, [taoShrink, taoFill], [taoShrink], 5, 5);
  ListViewTable.AddControlEx(3, 3, 1, 1, MoveDownButton, [taoShrink, taoFill], [taoShrink], 5, 5);
  ListViewTable.AddControlEx(3, 1, 1, 1, TGTKLabel.Create(Self), [taoShrink, taoFill], [taoExpand, taoFill], 0, 2);
  ListViewTable.AddControlEx(3, 4, 1, 1, TGTKLabel.Create(Self), [taoShrink, taoFill], [taoExpand, taoFill], 0, 2);

  AddColumnItems;

  ListView.OnSelectionChanged := ListViewSelectionChanged;
  MoveUpButton.OnClick := MoveUpDownButtonButtonClick;
  MoveDownButton.OnClick := MoveUpDownButtonButtonClick;

  ListView.SetFocus;
end;

procedure TFColumns.ListViewSelectionChanged(Sender: TObject);
begin
  try
    MoveUpButton.Enabled := Assigned(ListView.Selected) and (ListView.Selected.Index > 0);
    MoveDownButton.Enabled := Assigned(ListView.Selected) and (ListView.Selected.Index < ListView.Items.Count - 1);
  except end;
end;

procedure TFColumns.MoveUpDownButtonButtonClick(Sender: TObject);
var i, Old, New: integer;
    s: string;
    b: boolean;
begin
  if Assigned(ListView.Selected) then begin
    Old := ListView.Selected.Index;
    if (Sender = MoveUpButton) and (Old > 0) then New := Old - 1 else
    if (Sender = MoveDownButton) and (Old < ListView.Items.Count - 1) then New := Old + 1 else Exit;
    b := boolean(ListView.Items[New].AsInteger(0));
    ListView.Items[New].SetValue(0, integer(ListView.Items[Old].AsBoolean(0)));   //  Stupid but works
    ListView.Items[Old].SetValue(0, integer(b));
    s := ListView.Items[New].AsString(1);
    ListView.Items[New].SetValue(1, ListView.Items[Old].AsString(1));
    ListView.Items[Old].SetValue(1, s);
    i := ListView.Items[New].AsInteger(2);
    ListView.Items[New].SetValue(2, ListView.Items[Old].AsInteger(2));
    ListView.Items[Old].SetValue(2, i);
    ListView.Items[New].Selected := True;
    ListView.Items[New].SetCursor(1, False, not Application.GTKVersion_2_2_0_Up, 0.5, 0); 
  end;
end;

procedure TFColumns.AddColumnItems;
var Item: TGTKListItem;
    i: integer;
begin
  for i := 1 to 10 do begin
    Item := ListView.Items.Add;
    Item.SetValue(0, Integer(ConfColumnVisible[i]));
    Item.SetValue(1, ConfColumnTitlesLong[ConfColumnIDs[i]]);
    Item.SetValue(2, ConfColumnIDs[i]);
  end;
end;

procedure TFColumns.ListViewColumnToggled(Sender: TObject; Column: TGTKTreeViewColumn; Item: TGTKListItem);
begin
//  DebugMsg(['(II) TFColumns.ListViewColumnToggled: Item.AsBoolean(0) = ', Item.AsBoolean(0), ', Item.AsInteger(0) = ', Item.AsInteger(0)]);
  if Assigned(Item) then Item.SetValue(0, Integer(not Boolean(Item.AsInteger(0))));
end;


procedure TFColumns.ApplyColumnList;
var i, j: integer;
    TempIDs, TempArrayI: array[1..ConstNumPanelColumns] of integer;
    TempArrayB: array[1..ConstNumPanelColumns] of boolean;
begin
  //  Copy the old items
  for i := 1 to ConstNumPanelColumns do begin
   TempIDs[i] := ConfColumnIDs[i];
   TempArrayI[i] := ConfColumnSizes[i];
   TempArrayB[i] := ConfColumnVisible[i];
  end;

  //  Search for moved columns 
  for i := 0 to ListView.Items.Count - 1 do
    for j := 1 to ConstNumPanelColumns do
      if ListView.Items[i].AsInteger(2) = TempIDs[j] then begin
        ConfColumnIDs[i + 1] := ListView.Items[i].AsInteger(2);
        ConfColumnSizes[i + 1] := TempArrayI[j];
        ConfColumnVisible[i + 1] := boolean(ListView.Items[i].AsInteger(0));
        Break;
      end;
end;





end.

