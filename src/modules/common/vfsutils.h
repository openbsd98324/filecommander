/*  Tux Commander VFS: String utilities
 *  Copyright (C) 2007 Tomas Bzatek <tbzatek@users.sourceforge.net>
 *   Check for updates on tuxcmd.sourceforge.net
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __VFSUTILS_H__
#define __VFSUTILS_H__


#include <stdio.h>
#include <string.h>
#include <glib.h>

#include "vfs_types.h"


void copy_vfs_item(struct TVFSItem *src, struct TVFSItem *dst);
void free_vfs_item(struct TVFSItem *item);

int compare_two_same_files(const char *Path1, const char *Path2);




#endif /* __VFSUTILS_H__ */
