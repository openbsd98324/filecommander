/*  Tux Commander VFS: String utilities
 *  Copyright (C) 2007-2008 Tomas Bzatek <tbzatek@users.sourceforge.net>
 *   Check for updates on tuxcmd.sourceforge.net
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __TREEPATHUTILS_H__
#define __TREEPATHUTILS_H__


#include <stdio.h>
#include <string.h>
#include <glib.h>

#include "vfs_types.h"


struct PathTree {
  GPtrArray* items;
  struct TVFSItem *data;
  unsigned long index;
  char *node;
  char *original_pathstr;
};


struct PathTree* filelist_tree_new();
void filelist_tree_free(struct PathTree *tree);
void filelist_tree_print(struct PathTree *tree);

/*  Symlink resolving: strongly discouraged to use at the present state of art.
 *  We would have to implement full symlink system, do loop checking etc. */
void filelist_tree_resolve_symlinks(struct PathTree *tree);

gboolean filelist_tree_add_item(struct PathTree *tree, const char *path, const char *original_pathstr, struct TVFSItem *item, unsigned long index);
struct PathTree* filelist_tree_find_node_by_path(struct PathTree *tree, const char *path);
unsigned long int filelist_find_index_by_path(struct PathTree *tree, const char *path);
struct PathTree* filelist_tree_get_item_by_index(struct PathTree *tree, unsigned long index);






#endif /* __TREEPATHUTILS_H__ */
